-- Author      : Galilman
-- Create Date : 09/09/2014 18:21:21

local gxchat = gxchat;
local BottoneMinimappa = gxchat:NewModule("BottoneMinimappa", gxchat.prototipiModuli.gui, "AceTimer-3.0");

--Modulo chat gruppi
local moduloGruppi = gxchat:GetModule ("ChatGruppi");
local skinner = gxchat:GetModule ("Skinner");

--Localizzazione
local L = LibStub("AceLocale-3.0"):GetLocale("gxchat");

--LibDataBroker per Titan Panel
local LDB = LibStub:GetLibrary("LibDataBroker-1.1");

local db;
local bottone;
local messaggiDaLeggere;
local timerPartito;
local timer;
local blink = false;

local menu = {};
local frameMenu;

local dropDownGruppi = {};
local frameGruppi;

local timer;

--Valori default del db se non presente
local db_default = {
	--Modulo Abilitato
	abilitato = true,
	--Bottone Bloccato
	bloccato = false,
	--Angolo Bottone (posizione attorno alla minimappa)
	angolo = 225,
	--Distanza dal centro della minimappa
	raggio = 80,
	--Inizializzato il db
	inizializzato = true,
	--Mostra l'icona sulla minimappa
	mostraIcona = true;
}

--Finestra Opzioni
local opzioni = {
	name = "Minimappa",
	type = "group",
	handler = BottoneMinimappa,
	args = {
		visibile = {
			name = L["opzioni_minimappa_visibile"],
			type = "toggle",
			get = "GetAbilitato",
			set = "SetAbilitato",
			width = "full"
		},
		blocca = {
			name = L["opzioni_minimappa_blocca"],
			type = "toggle",
			get = "GetBloccato",
			set = "SetBloccato",
			width = "full"
		}
	}
};

local function posizionaBottone ()
	local angolo = math.rad (db.angolo or db_default.angolo);
	local raggio = db.raggio or db_default.raggio;

	local x, y = raggio * math.cos (angolo), raggio * math.sin (angolo);

	bottone:SetPoint("TOPLEFT", Minimap, "TOPLEFT", x + 54, y - 54);
end

local function Blink_Messaggi ()
	local skin = skinner:GetSkin ();
	if (blink) then
		blink = false;
		bottone.dataObject.icon = skin.icone.minimappa.normale;
		bottone.icon:SetTexture (bottone.dataObject.icon);
	else
		blink = true;
		bottone.dataObject.icon = skin.icone.minimappa.nuovoMessaggio;
		bottone.icon:SetTexture (bottone.dataObject.icon);
	end
end

local function Messaggio_DB_CHANGED ()
	if (not gxchat.db.profile.modules.BottoneMinimappa.inizializzato) then
		--BottoneMinimappa:Print ("Entro");
		gxchat.db.profile.modules.BottoneMinimappa = gxchat:copiaTable (db_default);
	end

	db = gxchat.db.profile.modules.BottoneMinimappa;
	bottone.dataObject.db = db;
	posizionaBottone ();

	if (db.mostraIcona) then
		bottone:Show ();
	else
		bottone:Hide ();
	end
end

local function Messaggio_SKIN_CAMBIATA ()
	local skin = skinner:GetSkin ();
	bottone.overlay:SetTexture (skin.icone.minimappa.bordi);
	bottone.background:SetTexture (skin.icone.minimappa.background);

	bottone.dataObject.icon = skin.icone.minimappa.normale;
	bottone.icon:SetTexture (bottone.dataObject.icon);
end

local function Messaggio_DEFAULT ()
end

local switchMessaggi =  {
	["DB_CHANGED"] = Messaggio_DB_CHANGED,
	["SKIN_CAMBIATA"] = Messaggio_SKIN_CAMBIATA
}

local function BottoneMinimappa_MessageCallback (message, ...)
	local func = switchMessaggi[message];
	if (func) then
		func (...);
	else
		Messaggio_DEFAULT ();
	end
end

local function Bottone_OnClick (self, button)
	if (button == "RightButton") then
		ToggleDropDownMenu(1, nil, frameMenu, self, 0, 0);
		--self:SendMessage ("APRI_OPZIONI");
	elseif (button == "LeftButton") then
		--BottoneMinimappa:SendMessage ("APRI_CHAT");
		ToggleDropDownMenu(1, nil, frameGruppi, self, 0, 0);
	end
end

local function Bottone_MostraTooltip (tt)
	tt:AddLine (string.format (L["titolo_tooltip_minimappa"], gxchat.versione));
	tt:AddLine (L["tooltip_minimappa_click_sinistro"]);
	tt:AddLine (L["tooltip_minimappa_click_destro"]);
	tt:AddLine (L["tooltip_minimappa_trascina"]);
	tt:AddLine (L["tooltip_minimappa_shift_trascina"]);
end

--Funzioni per creaBottoneDaLDB
--Aggiorna le coordinate dell'icona, se premuto viene portata in basso
local function Icona_AggiornaCoord (self)
	local defaultCoords = {0, 1, 0, 1};

	local coords = self:GetParent().dataObject.iconCoords or defaultCoords;
	local deltaX, deltaY = 0, 0;

	if (not self:GetParent().isMouseDown) then
		deltaX = (coords[2] - coords[1]) * 0.05;
		deltaY = (coords[4] - coords[3]) * 0.05;
	end

	self:SetTexCoord(coords[1] + deltaX, coords[2] - deltaX, coords[3] + deltaY, coords[4] - deltaY);
end

-- Tooltip code ripped from StatBlockCore by Funkydude
local function getAnchors (frame)
	local x, y = frame:GetCenter ();
	if (not x or not y) then 
		return "CENTER"; 
	end

	local hhalf = (x > UIParent:GetWidth()*2/3) and "RIGHT" or (x < UIParent:GetWidth()/3) and "LEFT" or "";
	local vhalf = (y > UIParent:GetHeight()/2) and "TOP" or "BOTTOM";
	return vhalf .. hhalf, frame, (vhalf == "TOP" and "BOTTOM" or "TOP") .. hhalf;
end

--Quando il mouse passa sopra il bottone, se non si muove e ha tooltip viene mostrato il tooltip
local function Bottone_OnEnter (self)
	if (self.isMoving) then
		return;
	end

	local obj = self.dataObject;

	if (obj.OnTooltipShow) then
		GameTooltip:SetOwner (self, "ANCHOR_NONE");
		GameTooltip:SetPoint (getAnchors(self));
		obj.OnTooltipShow (GameTooltip);
		GameTooltip:Show ();
	elseif obj.OnEnter then
		obj.OnEnter(self);
	end
end

--Quando il mouse esce dal bottone
local function Bottone_OnLeave (self)
	local obj = self.dataObject;
	GameTooltip:Hide ();

	if obj.OnLeave then 
		obj.OnLeave (self);
	end
end

--Chiamato a ogni frame
local function Bottone_OnUpdate (self)
	local mx = Minimap:GetLeft () + 70;
	local my = Minimap:GetBottom () + 70;
	local px, py = GetCursorPosition ();
	local scale = Minimap:GetEffectiveScale ();
	px, py = px / scale, py / scale;
	
	local angolo = math.deg (math.atan2 (py - my, px - mx)) % 360;
	local raggio = db.raggio or db_default.raggio;
	if (IsShiftKeyDown ()) then
		raggio = math.sqrt ((px - mx) ^ 2 + (py - my) ^ 2);
		raggio = math.max (raggio, db_default.raggio);
		raggio = math.min (raggio, 120);
	end
	
	--Salvo i valori
	self.dataObject.db.angolo = angolo;
	self.dataObject.db.raggio = raggio;

	angolo = math.rad (angolo);
	local x, y = raggio * math.cos (angolo), raggio * math.sin (angolo);

	self:SetPoint("TOPLEFT", Minimap, "TOPLEFT", x + 54, y - 54)
end

--Quando il bottone inizia a essere mosso
local function Bottone_OnDragStart (self)
	self:LockHighlight ();
	self.isMouseDown = true;
	self.icon:Icona_AggiornaCoord ();
	self:SetScript ("OnUpdate", Bottone_OnUpdate);
	self.isMoving = true;
	GameTooltip:Hide ();
end

local function Bottone_OnDragStop (self)
	self:SetScript ("OnUpdate", nil);
	self.isMouseDown = false;
	self.icon:Icona_AggiornaCoord ();
	self:UnlockHighlight ();
	self.isMoving = false;
end

local function Bottone_OnMouseDown (self)
	self.isMouseDown = true; 
	self.icon:Icona_AggiornaCoord ();
end

local function Bottone_OnMouseUp (self)
	self.isMouseDown = false; 
	self.icon:Icona_AggiornaCoord ();
end

local function creaBottoneDaLDB (LDBObject)
	local skin = skinner:GetSkin ();

	bottone = CreateFrame("Button", "gxchat_minimap_button", Minimap);
	bottone.dataObject = LDBObject;
	bottone:SetFrameStrata ("MEDIUM");
	bottone:SetSize (31, 31);
	bottone:SetFrameLevel (8);
	bottone:RegisterForClicks ("LeftButtonUp", "RightButtonUp");
	bottone:RegisterForDrag ("LeftButton");
	bottone:SetHighlightTexture (skin.icone.minimappa.hilight);

	local overlay = bottone:CreateTexture (nil, "OVERLAY");
	overlay:SetSize (53, 53);
	overlay:SetTexture (skin.icone.minimappa.bordi);
	overlay:SetPoint ("TOPLEFT");
	bottone.overlay = overlay;

	local background = bottone:CreateTexture (nil, "BACKGROUND");
	background:SetSize (20, 20);
	background:SetTexture (skin.icone.minimappa.background);
	background:SetPoint ("TOPLEFT", 6, -6);
	bottone.background = background

	local icon = bottone:CreateTexture (nil, "BORDER");
	icon:SetSize (20, 20);
	icon:SetTexture (LDBObject.icon);
	icon:SetPoint ("TOPLEFT", 6, -5);
	bottone.icon = icon;
	bottone.isMouseDown = false;
	bottone.icon = icon;

	local r, g, b = icon:GetVertexColor ();
	icon:SetVertexColor (LDBObject.iconR or r, LDBObject.iconG or g, LDBObject.iconB or b);

	icon.Icona_AggiornaCoord = Icona_AggiornaCoord;
	icon:Icona_AggiornaCoord ();

	bottone:SetScript ("OnEnter", Bottone_OnEnter);
	bottone:SetScript ("OnLeave", Bottone_OnLeave);
	if (LDBObject.OnClick) then
		bottone:SetScript ("OnClick", LDBObject.OnClick);
	end

	local db = LDBObject.db;

	if (not db or not db.bloccato) then
		bottone:SetScript ("OnDragStart", Bottone_OnDragStart);
		bottone:SetScript ("OnDragStop", Bottone_OnDragStop);
	end

	bottone:SetScript ("OnMouseDown", Bottone_OnMouseDown);
	bottone:SetScript ("OnMouseUp", Bottone_OnMouseUp);
end

local function menu_BloccaBottone ()
	db.bloccato = not db.bloccato;

	if (not db or not db.bloccato) then
		bottone:SetScript ("OnDragStart", Bottone_OnDragStart);
		bottone:SetScript ("OnDragStop", Bottone_OnDragStop);
	else
		bottone:SetScript ("OnDragStart", nil);
		bottone:SetScript ("OnDragStop", nil);
	end
end

local function menu_MostraBottone ()
	db.mostraIcona = not db.mostraIcona;

	if (db.mostraIcona) then
		bottone:Show ();
	else
		bottone:Hide ();
	end
end

local function menu_ApriOpzioni ()
	--BottoneMinimappa:SendMessage ("APRI_OPZIONI");
	--LibStub("AceConfigDialog-3.0"):Open ("gxchat opzioni");
	InterfaceOptionsFrame_OpenToCategory ("gxchat");
	InterfaceOptionsFrame_OpenToCategory ("gxchat");
end

local function InizializzaMenu (self, level)
	if (not level) then
		return;
	end

	wipe (menu);
	if (level == 1) then
		menu.text = string.format (L["titolo_tooltip_minimappa"], gxchat.versione);
		menu.isTitle = true;
		menu.notCheckable = true;
        UIDropDownMenu_AddButton (menu, level);

		--Blocca bottone
		menu.disabled     = false;
        menu.isTitle      = false;
        menu.notCheckable = false;
		menu.text = L["opzioni_minimappa_blocca_bottone"];
		menu.func = menu_BloccaBottone;
		menu.checked = db.bloccato;
		UIDropDownMenu_AddButton (menu, level);

		--Mostra icona
		menu.disabled     = false;
        menu.isTitle      = false;
        menu.notCheckable = false;
		menu.text = L["opzioni_minimappa_mostra_icona"];
		menu.func = menu_MostraBottone;
		menu.checked = db.mostraIcona;
		UIDropDownMenu_AddButton (menu, level);

		--Apri Opzioni
		menu.disabled     = false;
        menu.isTitle      = false;
        menu.notCheckable = true;
		menu.text = L["opzioni_minimappa_apri_opzioni"];
		menu.func = menu_ApriOpzioni;
		UIDropDownMenu_AddButton (menu, level);
	end
end

local function gruppi_ApriFinestraChat (self, gruppo)
	BottoneMinimappa:Debug ("gruppo = " .. gruppo);
	moduloGruppi:Apri (gruppo);
end

local function InizializzaGruppi (self, level)
	if (not level) then
		return;
	end

	wipe (dropDownGruppi);
	if (level == 1) then
		dropDownGruppi.text = string.format (L["titolo_tooltip_minimappa"], gxchat.versione);
		dropDownGruppi.isTitle = true;
		dropDownGruppi.notCheckable = true;
        UIDropDownMenu_AddButton (dropDownGruppi, level);
		
		local gruppi = moduloGruppi:getGruppi ();

		for i = 1, #gruppi do
			dropDownGruppi.text			= gruppi[i];
			if ((gruppi[i] == CHAT_MSG_OFFICER and not CanEditOfficerNote ()) or (gruppi[i] == CHAT_MSG_GUILD and not IsInGuild ())) then
				dropDownGruppi.disabled	= true;
			else
				dropDownGruppi.disabled = false
			end
			dropDownGruppi.isTitle		= false;
			dropDownGruppi.notCheckable = true;
			dropDownGruppi.arg1			= gruppi[i];
			dropDownGruppi.func			= gruppi_ApriFinestraChat;
			UIDropDownMenu_AddButton (dropDownGruppi, level);
		end
	end
end

local function creaBottone ()
	local skin = skinner:GetSkin ();
	local contenutoBottone = LDB:NewDataObject ("gxchat", {
		type = "launcher",
		icon = skin.icone.minimappa.normale,
		OnClick = Bottone_OnClick,
		OnTooltipShow = Bottone_MostraTooltip,
		db = db
	});

	creaBottoneDaLDB (contenutoBottone);

	posizionaBottone ()

	frameMenu = CreateFrame ("Frame", "gxchat_MenuMinimappa");
	frameMenu.displayMode = "MENU";
	frameMenu.initialize = InizializzaMenu;

	frameGruppi = CreateFrame ("Frame", "gxchat_FrameGruppi");
	frameGruppi.displayMode = "MENU";
	frameGruppi.initialize = InizializzaGruppi;
end

function BottoneMinimappa:OnInitialize ()
	if (not gxchat.db.profile.modules.BottoneMinimappa.inizializzato) then
		gxchat.db.profile.modules.BottoneMinimappa = gxchat:copiaTable (db_default);
	end

	db = gxchat.db.profile.modules.BottoneMinimappa;

	--creaBottone ();
	messaggiDaLeggere = 0;

	self:RegisterMessage ("DB_CHANGED", BottoneMinimappa_MessageCallback);
	self:RegisterMessage ("SKIN_CAMBIATA", BottoneMinimappa_MessageCallback);

	LibStub("AceConfig-3.0"):RegisterOptionsTable("gxchat_BottoneMinimappa", opzioni);
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("gxchat_BottoneMinimappa", L["gxchat_opzioni_BottoneMinimappa_titolo_Icona_Minimappa"], "gxchat");
end

--Abilito il modulo
function BottoneMinimappa:OnEnable ()
	if (not bottone) then
		creaBottone ();
	end

	if (db.mostraIcona) then
		self:Debug ("Bottone Visibile: " .. (bottone:IsVisible () and "si" or "no"));
		bottone:Show ();
	else
		bottone:Hide ();
	end
end

--Disabilito il modulo
function BottoneMinimappa:OnDisable()
	bottone:Hide ();

	self:UnregisterMessage ("NUOVO_MESSAGGIO");
	self:CancelTimer(timer);
	timerPartito = false;
end

--Presente in tutti i moduli per aggiornare le impostazioni dopo un aggiornamento
function BottoneMinimappa:AggiornaImpostazioni (versioneLocale)
	if (versioneLocale < 2002) then
		db.mostraIcona = db.abilitato;
		db.abilitato = true;
	end
end

function BottoneMinimappa:NuovoMessaggio ()
	timer = self:ScheduleRepeatingTimer(Blink_Messaggi, 1);
end

function BottoneMinimappa:StopBlink ()
	self:CancelTimer (timer);

	local skin = skinner:GetSkin ();
	bottone.dataObject.icon = skin.icone.minimappa.normale;
	bottone.icon:SetTexture (bottone.dataObject.icon);
end

function BottoneMinimappa:SetBloccato (info, bloccato)
	db.bloccato = bloccato;

	if (not db or not db.bloccato) then
		bottone:SetScript ("OnDragStart", Bottone_OnDragStart);
		bottone:SetScript ("OnDragStop", Bottone_OnDragStop);
	else
		bottone:SetScript ("OnDragStart", nil);
		bottone:SetScript ("OnDragStop", nil);
	end
end

function BottoneMinimappa:GetBloccato ()
	return db.bloccato;
end

function BottoneMinimappa:SetAbilitato (info, abilitato)
	db.mostraIcona = abilitato;

	if (db.mostraIcona) then
		bottone:Show ();
	else
		bottone:Hide ();
	end
end

function BottoneMinimappa:GetAbilitato ()
	return db.mostraIcona;
end