-- Author      : Galilman
-- Create Date : 21/07/2015 18:16:31

local basePath = "Interface\\Addons\\gxchat\\Interfaccia\\Immagini\\SkinChat\\Alliance\\";

local skin = {
	metadata = {
		autore = "Galilman",
		versione = "1.0"
	},
	backdrop = {
		frame = {
			sfondo = basePath .. "sfondoFrame.blp",
			bordi = basePath .. "bordoFrame.blp",
			spessoreBordi = 32,
			insets = {
				left = 5,
				right = 5,
				top = 5,
				bottom = 5
			}
		},
		messaggi = {
			sfondo = "Interface\\DialogFrame\\UI-DialogBox-Background",
			bordi = nil,
		},
		titolo = {
			sfondo = basePath .. "sfondoTitolo.blp",
			bordi = basePath .. "bordoFrame.blp",
			spessoreBordi = 32,
			insets = {
				left = 5,
				right = 5,
				top = 5,
				bottom = 5
			},
			coloreTesto = {1, 1, 1, 1};
		},
		textBox = {
			sfondo = "Interface\\DialogFrame\\UI-DialogBox-Background",
			bordi = basePath .. "bordoFrame.blp",
			spessoreBordi = 22,
			insets = {
				left = 5,
				right = 5,
				top = 5,
				bottom = 5
			},
		},
		frameGruppi = {
			sfondo = "Interface\\DialogFrame\\UI-DialogBox-Background",
			bordi = basePath .. "bordoFrame.blp",
			spessoreBordi = 22,
			insets = {
				left = 5,
				right = 5,
				top = 5,
				bottom = 5
			},
		},
		gruppo = {
			sfondo = "Interface\\DialogFrame\\UI-DialogBox-Background",
			sfondoSelezionato = "Interface\\DialogFrame\\UI-DialogBox-Gold-Background",
			bordi = basePath .. "bordoFrame.blp",
			spessoreBordi = 12,
			insets = {
				left = 5,
				right = 5,
				top = 5,
				bottom = 5
			},
		},
	},
	icone = {
		minimappa = {
			normale = basePath .. "icone\\iconaMinimappaNormale.blp",
			nuovoMessaggio = basePath .. "icone\\iconaMinimappaNuovoMessaggio.blp",
			bordi = "Interface\\Minimap\\MiniMap-TrackingBorder",
			hilight = "Interface\\Minimap\\UI-Minimap-ZoomButton-Highlight",
			background = "Interface\\Minimap\\UI-Minimap-Background"
		},
		chiudi = {
			normale = basePath .. "icone\\chiudiNormale.blp",
			premuto = basePath .. "icone\\chiudiPremuto.blp",
			evidenziato = basePath .. "icone\\chiudiEvidenziato.blp",
		},
		scrollUp = {
			normale = basePath .. "icone\\scrollUpNormale.blp",
			premuto = basePath .. "icone\\scrollUpPremuto.blp",
			evidenziato = basePath .. "icone\\scrollUpEvidenziato.blp",
			disabilitato = basePath .. "icone\\scrollUpDisabilitato.blp"
		},
		scrollDown = {
			normale = basePath .. "icone\\scrollDownNormale.blp",
			premuto = basePath .. "icone\\scrollDownPremuto.blp",
			evidenziato = basePath .. "icone\\scrollDownEvidenziato.blp",
			disabilitato = basePath .. "icone\\scrollDownDisabilitato.blp"
		},
		aggiungiGruppo = {
			normale = basePath .. "icone\\aggiungiNormale.blp",
			premuto = basePath .. "icone\\aggiungiPremuto.blp",
			evidenziato = basePath .. "icone\\chiudiEvidenziato.blp"
		},
		elimina = {
			normale = basePath .. "icone\\eliminaNormale.blp",
			premuto = basePath .. "icone\\eliminaPremuto.blp",
			evidenziato = "Interface\\QuestFrame\\UI-QuestLogTitleHighlight",
			disabilitato = basePath .. "icone\\eliminaDisabilitato.blp"
		}
	}
}

--Registro la skin su gxchat
gxchat:RegistraSkin ("Alliance", skin);