-- Author      : Galilman
-- Create Date : 29/09/2014 14:40:53

---------------------------------------------------------------------------------------
-- Controlla se i membri della gilda hanno l'addon attivo.
-- Disponibile solo agli officer
---------------------------------------------------------------------------------------

local gxchat = gxchat;
local ControlloMembri = gxchat:NewModule ("ControlloMembri", "AceComm-3.0", "AceTimer-3.0");

local L = LibStub("AceLocale-3.0"):GetLocale("gxchat");

local AceGUI = LibStub ("AceGUI-3.0"); 

--tabella player interpellati
local playerInterpellati = {};

local db
local mostrato = false;
local aggiornato = false;

local function Frame_OnClose (widget)
	AceGUI:Release (widget);
	mostrato = false;
end

local function MostraInterfaccia ()
	local frame = AceGUI:Create ("Frame");
	frame:SetTitle (L["gxchat_ControlloMembri_frame_title"]);
	frame:SetCallback ("OnClose", Frame_OnClose);
	frame:SetLayout ("Fill");

	local scroll = AceGUI:Create ("ScrollFrame");
	scroll:SetLayout ("List");
	frame:AddChild (scroll);

	for k, v in pairs (playerInterpellati) do
		if (gxchat:IsDebug ()) then
			ControlloMembri:Print ("Inserisco " .. k);
		end
		local label = AceGUI:Create ("Label");
		label:SetText (k);
		if (not v) then
			label:SetColor (1, 0, 0);
		elseif (v == "mobile") then
			label:SetColor (1, 0.75, 0);
		else
			label:SetColor (0.12, 1, 0.12);
		end
		scroll:AddChild (label);
	end
	mostrato = true;
end

local function ControlloMembri_GetRoster ()
	if (aggiornato) then
		return;
	end
	aggiornato = true;
	wipe (playerInterpellati);
	local numMembri = GetNumGuildMembers ();
	for i = 1, numMembri do
		local name, rank, rankIndex, level, class, zone, note, officernote, online, status, classFileName, achievementPoints, achievementRank, isMobile = GetGuildRosterInfo(i);
		if (online) then
			ControlloMembri:SendCommMessage ("gxchat", "Richiesta Addon Attivo", "WHISPER", name);
			playerInterpellati[name] = false;
			if (gxchat:IsDebug ()) then
				ControlloMembri:Print ("Inviato richiesta addon a: " .. name);
			end
		elseif (isMobile) then
			playerInterpellati[name] = "mobile";
		end
	end
	ControlloMembri:ScheduleTimer(MostraInterfaccia, 5)
end

local function ControlloMembri_MessageCallback (message, ...)
	if (message == "CONTROLLO_MEMBRI") then
		if (mostrato) then
			return;
		end
		aggiornato = false;
		ControlloMembri:RegisterEvent ("GUILD_ROSTER_UPDATE");
		GuildRoster ();
		ControlloMembri:ScheduleTimer (ControlloMembri_GetRoster, 1);
	end
end

function ControlloMembri:GUILD_ROSTER_UPDATE ()
	ControlloMembri:UnregisterEvent ("GUILD_ROSTER_UPDATE");
	if (gxchat:IsDebug ()) then
		self:Print ("Evento GUILD_ROSTER_UPDATE");
	end
	ControlloMembri_GetRoster ();
	aggiornato = true;
end

function ControlloMembri:OnInitialize ()
	db = gxchat.db.profile.modules.ControlloMembri;
	db.abilitato = true;
	db.inizializzato = true;

	self:RegisterComm ("gxchat", "OnCommReceived");
end

function ControlloMembri:OnEnable ()
	if (not IsInGuild ()) then
		return;
	end
	if (CanViewOfficerNote ()) then --O forse meglio CanEditOfficerNote () ???
		self:RegisterMessage ("CONTROLLO_MEMBRI", ControlloMembri_MessageCallback);
	end
end

function ControlloMembri:OnDisable()
end

function ControlloMembri:OnCommReceived (prefix, message, distribution, sender)
	if (prefix == "gxchat") then
		if (message == "Richiesta Addon Attivo") then
			self:SendCommMessage ("gxchat", "Risposta Addon Attivo: si", "WHISPER", sender);
			if (gxchat:IsDebug ()) then
				self:Print ("Inviato risposta addon attivo a: " .. sender);
			end
		elseif (message == "Risposta Addon Attivo: si") then
			for k, v in pairs (playerInterpellati) do
				if string.find (k, sender) then
					playerInterpellati[k] = true;
				end
			end
			if (gxchat:IsDebug ()) then
				self:Print ("Ricevuto risposta addon attivo da: " .. sender);
			end
		end
	end
end

--Presente in tutti i moduli per aggiornare le impostazioni dopo un aggiornamento
function ControlloMembri:AggiornaImpostazioni (versioneLocale)
	self:Debug (versioneLocale);
end