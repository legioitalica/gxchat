-- Author      : Galilman
-- Create Date : 19/09/2014 13:22:02

local gxchat = gxchat;
local GestioneGilde = gxchat:NewModule("GestioneGilde", "AceComm-3.0");

local L = LibStub("AceLocale-3.0"):GetLocale("gxchat");

local AceConfigDialog = LibStub("AceConfigDialog-3.0")
local AceConfigRegistry = LibStub("AceConfigRegistry-3.0")
local AceConfig = LibStub("AceConfig-3.0")
local AceGUI = LibStub ("AceGUI-3.0");

--Librerie serializzazione gilde associate
local libS = LibStub:GetLibrary ("AceSerializer-3.0");
local libEnc = LibStub:GetLibrary ("gxchat-LibCodifiche");

local tabOpzioni = {
	name = L["gxchat_GestioneGilde_FrameHeader"],
	type = "group",
	args = {}
}

local gildeAssociate = {};
local tabellaServer = {};

local tempNome, tempServer;
local editNomeNew, editServerNew, editInfo;

local MAX_INFO_CHAR = 500;

-- Callback OnClose frame errore, rilascia il frame e tutto il suo contenuto cos� da poter essere usato da altri addon
local function errorFrame_OnColse (widget)
	AceGUI:Release (widget);
end

--Crea frame errore con testo specificato
local function creaFrameErrore (txtErrore)
	local errorFrame = AceGUI:Create ("Frame");
	errorFrame:SetTitle (L["gxchat_error_frame_title"]);
	errorFrame:SetCallback ("OnClose", errorFrame_OnColse);
	errorFrame:SetLayout ("Fill");
	
	local testo = AceGUI:Create ("Label");
	testo:SetText (txtErrore);
	testo:SetFontObject (GameFontNormalLarge);
	errorFrame:AddChild (testo);
	PlaySound ("RaidWarning");
end

---------------------------------------------------------------------------------------
-- Salva le gilde associate sulle note di gilda
-- Se le nuove note hanno troppi caratteri mostra un frame di errore
---------------------------------------------------------------------------------------
local function scriviGildeSuInfo ()
	local info = GetGuildInfoText ();
	local gxStart, gxEnd = string.find (info, "## gxchat");
	if (gxStart) then
		info = string.sub (info, 1, gxStart - 3);
	end

	local strGilde = libS:Serialize (gildeAssociate);
	strGilde = libEnc:ToBase64 (strGilde);
	info = info .. "\n\n## gxchat\n" .. strGilde;

	if (string.len (info) >= MAX_INFO_CHAR) then
		local error = string.format (L["gxchat_GestioneGilde_error_frame_superati_caratteri_massimi"], string.len (info));
		creaFrameErrore (error);
		return;
	end

	SetGuildInfoText (info);

	GestioneGilde:SendCommMessage ("gxchat", "GILDE_CAMBIATE", "GUILD");
	GestioneGilde:SendMessage ("GILDE_CAMBIATE");
end

-- Dichiarazione funzione per aggiungere le gilde alla finestra gilde associate
local InserisciGilde;

-- Aggiunge una gilda alle gilde associate
local function AggiungiGilda ()
	if (tempNome and tempServer) then
		gildeAssociate [tempNome .. "@" .. tempServer] = true;
		scriviGildeSuInfo ();
		InserisciGilde ();
	end
end

-- Modifica la gilda associata selezionata
local function modificaGilda (info)
	gildeAssociate[info[1]] = nil;
	gildeAssociate[editNomeNew .. "@" .. editServerNew] = true;
	editInfo = nil;
	editNomeNew = nil;
	editServerNew = nil;
	scriviGildeSuInfo ();
	InserisciGilde ();
end

-- Elimina la gilda associata
local function eliminaGilda (info)
	gildeAssociate[info[1]] = nil;
	scriviGildeSuInfo ();
	InserisciGilde ();
end

---------------------------------------------------------------------------------------
-- Get e Set per dati gilde associate nel frame
---------------------------------------------------------------------------------------
local function getEditNome (info)
	if (not editNomeNew or not editInfo) then
		editNomeNew = gxchat:split(info[1], "@")[1];	
	end

	return editNomeNew;
end

local function setEditNome (info, value)
	editNomeNew = value;
	editInfo = info;
end

local function getEditServer (info) 
	if (not editServerNew or not editInfo) then
		editServerNew = gxchat:split(info[1], "@")[2];
	end

	return gxchat.realms[editServerNew];
end

local function setEditServer (info, value)
	editServerNew = tabellaServer[value];
	editInfo = info;
end

local function setNuovoNome (info, value)
	tempNome = value;
end

local function getNuovoNome ()
	return tempNome;
end

local function setNuovoServer (info, value)
	tempServer = tabellaServer[value];
end

local function getNuovoServer ()
	return gxchat.realms[tempServer];
end

---------------------------------------------------------------------------------------
-- Implementazione funzione inserimento gilde nel frame
---------------------------------------------------------------------------------------
InserisciGilde = function ()
	wipe (gildeAssociate);
	if (IsInGuild ()) then
		
		local messaggioGilda = GetGuildInfoText ();
		local gxStart, gxEnd = string.find (messaggioGilda, "## gxchat");
		if (gxEnd) then
			local gx = string.sub (messaggioGilda, gxEnd + 2);
			gx = libEnc:FromBase64 (gx);
			local succ, des = libS:Deserialize (gx);
			if (succ) then
				gildeAssociate = des;
			else
				GestioneGilde:Print (des);
			end
		end

	end

	local args = tabOpzioni.args;
	wipe (args);

	args["descrizione"] = {
		order = 1,
		type = "description",
		name = L["gxchat_GestioneGilde_Frame_Descrizione"]
	};

	args["nuova_gilda"] = {
		order = 2,
		name = L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Header"],
		type = "group",
		inline = true,
		args = {}
	};

	local ng_args = args["nuova_gilda"].args;
	ng_args["nome_gilda"] = {
		order = 1,
		type = "input",
		name = L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Nome"],
		get = getNuovoNome,
		set = setNuovoNome
	};
	
	ng_args["server"] = {
		order = 2,
		name = L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Server"],
		type = "select",
		values = tabellaServer,
		style = "dropdown",
		get = getNuovoServer,
		set = setNuovoServer
	};

	args["aggiungi"] = {
		name = L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Pulsante_Aggiungi"],
		order = 4,
		type = "execute",
		func = AggiungiGilda
	};

	args["gilde"] = {
		order = 5,
		name = L["gxchat_GestioneGilde_Frame_Gilde"],
		type = "header"
	};

	for k, v in pairs (gildeAssociate) do
		args[k] = {
			name = k,
			type = "group",
			inline = false,
			args = {
				nome = {
					order = 1,
					type = "input",
					name = L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Nome"],
					get = getEditNome,
					set = setEditNome
				},
				server = {
					order = 2,
					name = L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Server"],
					type = "select",
					values = tabellaServer,
					style = "dropdown",
					get = getEditServer,
					set = setEditServer
				},
				salva_modifiche = {
					order = 3,
					name = L["gxchat_GestioneGilde_Frame_Salva_Modifiche"],
					type = "execute",
					func = modificaGilda
				},
				elimina_gilda = {
					order = 4,
					name = L["gxchat_GestioneGilde_Frame_Elimina_Gilda"],
					type = "execute",
					func = eliminaGilda
				}
			}
		};
	end

	AceConfigRegistry:NotifyChange ("gxchat_GestioneGilde");
end

---------------------------------------------------------------------------------------
-- Gestione messaggi addon
---------------------------------------------------------------------------------------
-- Gestione messaggio apertura frame
local function Message_GESTIONE_GILDE_OPEN_FRAME ()
	if (CanEditGuildInfo ()) then
		AceConfigDialog:Open ("gxchat_GestioneGilde");
		InserisciGilde ();
	else
		creaFrameErrore (L["gxchat_GestioneGilde_error_frame_impossibile_salvare"]);
	end
end

-- Tabella switch messaggi
local switchTable = {
	["GESTIONE_GILDE_OPEN_FRAME"] = Message_GESTIONE_GILDE_OPEN_FRAME
}

-- Callback ricezione messaggio, si occupa di chiamare la funzione corrispondente al messaggio arrivato
local function GestioneGilde_MessageCallback (message, ...)
	local func = switchTable[message];
	if (func) then
		func (...);
	end
end

-- Inizializzazione Modulo
function GestioneGilde:OnInitialize ()
	self:RegisterMessage ("GESTIONE_GILDE_OPEN_FRAME", GestioneGilde_MessageCallback);

	for k, v in pairs (gxchat.realms) do
		tabellaServer[v] = k;
	end

	InserisciGilde ();
	AceConfig:RegisterOptionsTable ("gxchat_GestioneGilde", tabOpzioni);
	self:RegisterComm ("gxchat", "OnCommReceived");
end

function GestioneGilde:OnEnable ()
end

function GestioneGilde:OnDisable()
end

-- Ricezione AddonMessage, si occupa di avvertire i vari moduli del cambiamento di gilde associate
function GestioneGilde:OnCommReceived (prefix, message, distribution, sender)
	if (prefix == "gxchat") then
		if (message == "GILDE_CAMBIATE") then
			self:SendMessage ("GILDE_CAMBIATE");
		end
	end
end