﻿-- Author      : Galilman
-- Create Date : 21/03/2015 18:08:25

local gxchat = gxchat;
local ChatGruppi = gxchat:NewModule("ChatGruppi", gxchat.prototipiModuli.gui);

local libComunicazione = LibStub:GetLibrary("gxchat-LibComunicazione");

--Localizzazione
local L = LibStub("AceLocale-3.0"):GetLocale("gxchat");

--Libreria per condivisione media
local libSM = LibStub:GetLibrary("LibSharedMedia-3.0");

--Libreria emoticons
local libEmoticons = LibStub:GetLibrary ("gxchat-LibEmoticons");

--Libreria audio
local libSharedMedia = LibStub:GetLibrary ("LibSharedMedia-3.0");

--Modulo per le skin
local skinner = gxchat:GetModule ("Skinner");

local db;
local dbGruppi;
local dbGruppiCopia;

local playerName;
local classe;

local gui;
local offset = 0;

local gruppi = {};

local gruppoCorrente = "";

--Valori default del db se non presente
local db_default = {
	--Modulo Abilitato
	abilitato = true,
	--Inizializzato il db
	inizializzato = true,
	--Dati frame
	frame = {
		posizione = {
			x = 0,
			y = 0,
			punto = "CENTER",
			relativoA = "UIParent",
			ancoraggio = "CENTER"
		},
		dimensioni = {
			larghezza = 584,
			altezza = 256
		}
	},
	messaggio = {
		coloreMessaggio = {
			r = 1,
			g = 1,
			b = 1,
			a = 1
		},
		font = "Arial Narrow",
		fontSize = 12,
		formato = {
			emoticons = false,
			stile_emoticons = "",
			data = "%H:%M",
			server = true
		}
	},
	audio = {
		attivo = true,
		suono = "[gxchat] Campanello"
	}
}

--Valori default gruppo
local gruppo_default = {
	--Offset nello ScrollingMessageFrame, inizializzato con GetCurrentScroll (), all'apertura del gruppo viene effetuato uno scroll con SetScrollOffset(offset)
	offset = 0,
	--Id ultimo messaggio ricevuto, è la posizione all'interno del vettore messaggi
	idUltimoMessaggio = 0,
	--Messaggi da leggere
	daLeggere = 0,
	--Vettore contenente i messaggi.
	messaggi = {
		--[1] = {
			--Chi ha inviato il messaggio
			--sender = "",
			--Classe di sender
			--classe = "",
			--Messaggio
			--messaggio = "",
			--Timestamp di quando è stato ricevuto
			--time = 123
		--}
	}
}

--PopupDialog aggiunta gruppi
local popupDialog = {
	text = L["gxchat_ChatGruppi_aggiungi_gruppo_titolo"],
	button1 = L["gxchat_ChatGruppi_aggiungi_gruppo_aggiungi"],
	button2 = L["gxchat_ChatGruppi_aggiungi_gruppo_annulla"],
	whileDead = true,
	hideOnEscape = true,
	preferredIndex = 3,
	hasEditBox = true,
	OnAccept = function (self, data, data2)
		local text = self.editBox:GetText();
		if (gruppi[text]) then
			return;
		end
		gruppi[text] = gxchat:copiaTable (gruppo_default);
		table.insert (dbGruppi, text);
		table.insert (dbGruppiCopia, text);
		gui.frameGruppi.Update ();
	end,
	EditBoxOnTextChanged = function (self, data)   -- careful! 'self' here points to the editbox, not the dialog
		if (data and string.len (data) > 0) then
			self:GetParent().button1:Enable();         -- self:GetParent() is the dialog
		end
	end
}

--Formati data
local formatiData = {
	["%H:%M"] = "hh:mm",
	["%H:%M:%S"] = "hh:mm:ss",
	["%d-%m-%Y %H:%M"] = "dd-mm-aaaa hh:mm",
	["%d-%m-%Y %H:%M:%S"] = "dd-mm-aaaa hh:mm:ss",
	["%d-%m-%y %H:%M"] = "dd-mm-aa hh:mm",
	["%d-%m-%y %H:%M:%S"] = "dd-mm-aa hh:mm:ss"
}

--Pagina opzioni
local opzioni = {
	name = "ChatGruppi",
	type = "group",
	handler = ChatGruppi,
	args = {
		gruppoFont = {
			name = L["opzioni_ChatGruppi_font_header"],
			type = "group",
			args = {
				font = {
					name = L["opzioni_ChatGruppi_font"],
					type = "select",
					dialogControl = 'LSM30_Font',
					desc = L["opzioni_ChatGruppi_font_desc"],
					values = libSM:HashTable("font"),
					get = "GetFont",
					set = "SetFont",
					width = "full",
					order = 0
				},
				fontSize = {
					name = L["opzioni_ChatGruppi_dimensione_font"],
					type = "range",
					min = 8,
					max = 48,
					step = 1,
					get = "GetFontSize",
					set = "SetFontSize",
					width = "full",
					order = 1
				},
				fontColor = {
					name = L["opzioni_ChatGruppi_colore_font"],
					type = "color",
					get = "GetFontColor",
					set = "SetFontColor",
					hasAlpha = true,
					width = "full",
					order = 2
				}
			},
		},
		gruppoFormato = {
			name = L["opzioni_ChatGruppi_formato_header"],
			type = "group",
			args = {
				emoticons = {
					name = L["opzioni_ChatGruppi_formato_emoticons"],
					type = "toggle",
					order = 1,
					disabled = function () 
						return not libEmoticons:HasStyles ();
					end,
					desc = function ()
						if (libEmoticons:HasStyles ()) then
							return "";
						else
							return L["opzioni_ChatGruppi_formato_emoticons_disabilitato"];
						end
					end,
					get = "GetEmoticons",
					set = "SetEmoticons"
				},
				stile_emoticons = {
					name = L["opzioni_ChatGruppi_formato_emoticons_stile"],
					type = "select",
					order = 2,
					values = libEmoticons:GetStyles (),
					disabled = function () 
						return not ChatGruppi:GetEmoticons(); 
					end,
					get = "GetStyle",
					set = "SetStyle"
				},
				dropdown = {
					name = L["gxchat_opzioni_ChatGruppi_data"],
					type = "select",
					values = formatiData,
					order = 3,
					width = "full",
					get = "GetFormatoData",
					set = "SetFormatoData"
				},
				server = {
					name = L["gxchat_opzioni_ChatGruppi_mostra_server"],
					type = "toggle",
					order = 4,
					width = "full",
					get = "GetServer",
					set = "SetServer"
				},
				header = {
					name = L["opzioni_ChatGruppi_formato_anteprima"],
					order = 10,
					width = "full",
					type = "header"
				},
				anteprima = {
					name = function (info)
						local msg = "Test 123 :)";
						if (ChatGruppi:GetEmoticons ()) then
							msg = libEmoticons:FormatMessage (msg, ChatGruppi:GetStyle ());
						end
						local guildName, guildRankName, guildRankIndex = GetGuildInfo("player");
						local formatoGilda = date (ChatGruppi:GetFormatoData ()) .. " [" .. (guildName or (CHAT_MSG_GUILD .. " Test"));
						if (ChatGruppi:GetServer ()) then
							formatoGilda = formatoGilda .. "@" .. GetRealmName ();
						end
						formatoGilda = formatoGilda .. "][" .. UnitName ("player") .. "] " .. msg;
						local formatoOfficer = date (ChatGruppi:GetFormatoData ()) .. " [" .. (guildName or (CHAT_MSG_GUILD .. " Test"));
						if (ChatGruppi:GetServer ()) then
							formatoOfficer = formatoOfficer .. "@" .. GetRealmName ();
						end
						formatoOfficer = formatoOfficer .. "][Officer " .. UnitName ("player") .. "] " .. msg;
						local formatoGruppi = date (ChatGruppi:GetFormatoData ()) .. "[" .. UnitName ("player") .. "] " .. msg;
						return CHAT_MSG_GUILD .. ":\n" .. formatoGilda .. "\n" .. CHAT_MSG_OFFICER .. ":\n" .. formatoOfficer .. "\n" .. L["opzioni_ChatGruppi_formato_anteprima_gruppi"] .. ":\n" .. formatoGruppi;
					end,
					type = "description",
					order = 11,
					width = "full"
				}
			}
		},
		gruppoAudioVideo = {
			name = L["opzioni_ChatGruppi_audio_video_header"],
			type = "group",
			args = {
				avviso = {
					name = L["opzioni_ChatGruppi_audio_attivo"],
					type = "toggle",
					get = "GetAudioAttivo",
					set = "SetAudioAttivo",
					order = 0
				},
				suono = {
					name = L["opzioni_ChatGruppi_audio_suono"],
					type = "select",
					dialogControl = 'LSM30_Sound',
					values = libSharedMedia:HashTable("sound"),
					disabled = function () 
						return not ChatGruppi:GetAudioAttivo (); 
					end,
					get = "GetSuono",
					set = "SetSuono",
					order = 1
				}
			}
		}
	}
}

---------------------------------------------------------------------------------------
-- Funzioni Locali
---------------------------------------------------------------------------------------
local function find (tab, val)
	for k, v in pairs (tab) do
		if (v == val) then
			return k;
		end
	end
	return 0;
end

local function contains (tab, val)
	if (find (tab, val) == 0) then
		return false;
	else
		return true;
	end
end

--Restituisce il nome con il colore della classe
local function GetColoredName (name, class) 
	ChatGruppi:Debug ("classe: " .. class);
	if (class) then
		local classColorTable = RAID_CLASS_COLORS[class];
		if (not classColorTable) then
			return name;
		end
		return string.format("|cff%.2x%.2x%.2x", classColorTable.r*255, classColorTable.g*255, classColorTable.b*255) .. name .. "|r";
	end
	return name;
end

local function caricaMessaggi ()
	if (not gui:IsShown ()) then
		return;
	end
	gui.msgFrame:Clear ();

	gruppi[gruppoCorrente].daLeggere = 0;

	local msg = gruppi[gruppoCorrente].messaggi;
	for k, v in pairs (msg) do
		mess = v.messaggio;
		if (db.messaggio.formato.emoticons) then
			mess = libEmoticons:FormatMessage (mess, db.messaggio.formato.stile_emoticons);
		end
		if (gruppoCorrente == CHAT_MSG_GUILD) then
			if (v.tipo == "msg") then
				gui.msgFrame:AddMessage (date (db.messaggio.formato.data, v.time) .. " [" .. v.gilda .. (db.messaggio.formato.server and ("@" .. v.server)) .. "]" .. " [" .. GetColoredName (v.sender, v.classe) .. "] " .. mess);
			else
				gui.msgFrame:AddMessage (date (db.messaggio.formato.data, v.time) .. " [" .. v.gilda .. (db.messaggio.formato.server and ("@" .. v.server)) .. "]" .. string.format (v.messaggio, v.sender));
			end
		elseif (gruppoCorrente == CHAT_MSG_OFFICER) then
			gui.msgFrame:AddMessage (date (db.messaggio.formato.data, v.time) .. " [" .. v.gilda .. (db.messaggio.formato.server and ("@" .. v.server)) .. "]" .. " [" .. GetColoredName (v.sender, v.classe) .. "] " .. mess);
		else
			gui.msgFrame:AddMessage (date ("%H:%M:%S", v.time) .. " [" .. GetColoredName (v.sender, v.classe) .. "] " .. mess);
		end
	end

	for k,v in pairs (gruppi) do
		if (v.daLeggere > 0) then
			return;
		end
	end
	gxchat:GetModule ("BottoneMinimappa"):StopBlink ();
end
---------------------------------------------------------------------------------------
-- Funzioni Supporto Frame
---------------------------------------------------------------------------------------
--Salvo il frame
local function salvaFrame ()
	--Posizione del frame sullo schermo
	local point, relativeTo, relativePoint, xOfs, yOfs = gui:GetPoint ();
	db.frame.posizione.x			= xOfs;
	db.frame.posizione.y			= yOfs;
	db.frame.posizione.ancoraggio	= relativePoint;
	db.frame.posizione.punto		= point;
	if (relativeTo) then
		db.frame.posizione.relativoA = relativeTo:Name ();
	else
		db.frame.posizione.relativoA = "UIParent";
	end
	--Dimensione del frame
	db.frame.dimensioni.larghezza	= gui:GetWidth ();
	db.frame.dimensioni.altezza		= gui:GetHeight ();
end

--Carico il frame
local function caricaFrame ()
	--Carico stato frame
	local xOfs			= db.frame.posizione.x;
	local yOfs			= db.frame.posizione.y;
	local point			= db.frame.posizione.punto;
	local relativeTo	= db.frame.posizione.relativoA;
	local relativePoint	= db.frame.posizione.ancoraggio;
	
	if (xOfs == nil or yOfs == nil or point == nil or relativePoint == nil or relativeTo) then
		return;
	end

	gui:ClearAllPoints ();
	gui:SetPoint (point, relativeTo, relativePoint, xOfs, yOfs);

	gui:SetWidth (db.frame.dimensioni.larghezza);
	gui:SetHeight (db.frame.dimensioni.altezza);

	-- Imposto il font
	gui.msgFrame:SetFont (libSM:Fetch ("font", db.messaggio.font), db.messaggio.fontSize);
	gui.msgFrame:SetTextColor (db.messaggio.coloreMessaggio.r, db.messaggio.coloreMessaggio.g, db.messaggio.coloreMessaggio.b, db.messaggio.coloreMessaggio.a);
end

--Abilito/Disabilito bottoni scroll messaggio
local function gui_ScrollButton ()
	local scrollUp = gui.bottoneScrollUpMessaggi;
	local scrollDown = gui.bottoneScrollDownMessaggi;

	if (gui.msgFrame:AtTop ()) then
		scrollUp:Disable ();
	else
		scrollUp:Enable ();
	end

	if (gui.msgFrame:AtBottom ()) then
		scrollDown:Disable ();
	else
		scrollDown:Enable ();
	end
end
		
--Quando viene aperta la finestra
local function gui_OnShow ()
	local index = find (dbGruppiCopia, gruppoCorrente);

	--Nessun controllo sull'offset dato che il gruppo dovrebbe esserci
	offset = index - 1;

	gui.frameGruppi.Update ();
	caricaMessaggi ();
	gui_ScrollButton ();
end

--Mouse rilasciato su frame principale
local function gui_OnMouseUp () 
	if (gui.stateMove) then
		if (gui.isMoving) then
			gui:StopMovingOrSizing ();
			salvaFrame ();
			gui.isMoving = false;
		end
	end
end

--Mouse premuto su frame principale
local function gui_OnMouseDown (self, button)
	if (gui.stateMove) then
		if (button == "LeftButton") then
			gui:StartMoving ();
			gui.isMoving = true;
		end
	end
end

--Inizio movimento finestra
local function gui_OnDragStart ()
	gui:StartMoving ();
	gui.isMoving = true;
end

--Fine movimento finestra
local function gui_OnDragStop ()
	gui:StopMovingOrSizing();
    gui.isMoving = false;

	salvaFrame ();
end

-- Click collegamento
local function Messaggi_OnHyperlinkClick (self, linkData, link, button)
	ChatFrame_OnHyperlinkShow (self, linkData, link, button);
end

-- Scroll rotellina mouse
local function Messaggi_OnMouseWheel (self, delta)
	if (delta > 0) then
		gui.msgFrame:ScrollUp ();
	elseif (delta < 0) then
		gui.msgFrame:ScrollDown ();
	end

	ChatGruppi:Debug (gui.msgFrame:GetCurrentScroll ());
	gruppi[gruppoCorrente].offset = gui.msgFrame:GetCurrentScroll ();
	gui_ScrollButton ();
end

--Click su bottone scrollup messaggi
local function BottoneScrollUpMessaggi_OnClick ()
	local m = gui.msgFrame;
	if (IsControlKeyDown ()) then
		m:ScrollToTop ();
	elseif (IsShiftKeyDown ()) then
		m:PageUp ();
	else
		m:ScrollUp ();
	end

	gui_ScrollButton ();
end

--Click su bottone scrolldown messaggi
local function BottoneScrollDownMessaggi_OnClick ()
	local m = gui.msgFrame;
	if (IsControlKeyDown ()) then
		m:ScrollToBottom ();
	elseif (IsShiftKeyDown ()) then
		m:PageDown ();
	else
		m:ScrollDown ();
	end

	gui_ScrollButton ();
end

--Click sul bottone chiusura
local function BottoneChiudi_OnClick ()
	gui:Hide ();
end

--Invio sulla textbox
local function gui_TextBox_OnEnterPressed (self)
	local testo = self:GetText ();
	if (string.len (testo) > 0) then
		if (gruppoCorrente == CHAT_MSG_GUILD) then
			SendChatMessage (testo, "GUILD");
		elseif (gruppoCorrente == CHAT_MSG_OFFICER) then
			SendChatMessage (testo, "OFFICER");
		else
			local msg = {};
			msg.player = playerName;
			msg.messaggio = testo;
			msg.classe = classe;
			msg.gruppo = gruppoCorrente;
			libComunicazione:SendMessage ("gxchat_chat_gruppi", msg);
			ChatGruppi:AggiungiMessaggio (gruppoCorrente, msg);
			ChatGruppi:Debug ("Invio " .. msg.messaggio .. " su " .. msg.gruppo);
		end
	end
	self:SetText ("");
	self:Hide ();
	self:Show ();
end

--Pressione esc quando la textbox ha focus
local function gui_TextBox_OnEscapePressed (self)
	self:SetText ("");
	self:Hide ();
	self:Show ();
end

--Scroll rotellina mouse gruppi
local function  Gruppi_OnMouseWheel (self, delta, stepSize)
	-- delta > 0: su
	-- delta < 0: giù
	offset = offset - delta * 5;

	if (offset < 0) then
		offset = 0;
	elseif (offset > #dbGruppiCopia - 1) then
		offset = #dbGruppiCopia - 1;
	end

	gui.frameGruppi.Update ();
end

--Click su bottone scroll up dei gruppi
local function BottoneScrollUpGruppi_OnClick ()
	--Scroll su del frame gruppi
	Gruppi_OnMouseWheel (gui.frameGruppi, 1);
end

--Click su bottone scroll down dei gruppi
local function BottoneScrollDownGruppi_OnClick ()
	--Scroll giù del frame gruppi
	Gruppi_OnMouseWheel (gui.frameGruppi, -1);
end

--Click su bottone aggiungi gruppo
local function BottoneAggiungiGruppo_OnClick ()
	StaticPopup_Show ("gxchat_popup_gruppi");
end

local function Gruppi_Update (...)
	local frameGruppi = gui.frameGruppi;

	local bottoni = frameGruppi.buttons;
	local bottoniElimina = frameGruppi.bottoniElimina;

	local skin = skinner:GetSkin ();

	for i = 1, #bottoni do
		local index = i + offset;
		--Parto con i bottoni attivi e nascosti
		bottoni[i]:Hide ();
		bottoniElimina[i]:Hide ();

		bottoni[i]:Enable ();
		bottoniElimina[i]:Enable ();
		if (index <= #dbGruppiCopia) then
			--Al bottone corrisponde un gruppo, imposto i dati
			bottoni[i]:SetID (index);
			bottoni[i]:SetText (dbGruppiCopia[index]);
			bottoni[i]:Show ();

			bottoniElimina[i]:SetID (index);
			bottoniElimina[i]:Show ();

			--Se il bottone corrisponde al gruppo selezionato lo evidenzio;
			if (dbGruppiCopia[index] == gruppoCorrente) then
				bottoni[i]:SetBackdrop ({
					bgFile = skin.backdrop.gruppo.sfondoSelezionato,
					edgeFile = skin.backdrop.gruppo.bordi,
					tile = false,
					tileSize = 0,
					edgeSize = skin.backdrop.gruppo.spessoreBordi or 32, 
					insets = skin.backdrop.gruppo.insets or {
						left = 0,
						right = 0,
						top = 0,
						bottom = 0
					}
				});
			else
				bottoni[i]:SetBackdrop ({
					bgFile = skin.backdrop.gruppo.sfondo,
					edgeFile = skin.backdrop.gruppo.bordi,
					tile = false,
					tileSize = 0,
					edgeSize = skin.backdrop.gruppo.spessoreBordi or 32, 
					insets = skin.backdrop.gruppo.insets or {
						left = 0,
						right = 0,
						top = 0,
						bottom = 0
					}
				});
			end

			--Il "gruppo" gilda non si può eliminare, quindi disattivo il bottone elimina
			if (index == 1) then
				bottoniElimina[i]:Disable ();
				--Se non è in gilda disattivo anche il bottone per accedere alla chat
				if (not IsInGuild ()) then
					bottoni[i]:Disable ();
				end
			--Il "gruppo" officer non si può eliminare, quindi disattivo il bottone elimina 
			elseif (index == 2) then
				bottoniElimina[i]:Disable ();
				--Se non può editare le note degli officer non può accedere alla chat degli officer
				if (not CanEditOfficerNote ()) then
					bottoni[i]:Disable ();
				end
			end
		end
	end

	if (offset == 0) then
		gui.bottoneScrollUpGruppi:Disable ();
	else
		gui.bottoneScrollUpGruppi:Enable ();
	end

	if (offset >= #dbGruppiCopia - 1) then
		gui.bottoneScrollDownGruppi:Disable ();
	else
		gui.bottoneScrollDownGruppi:Enable ();
	end
end

local function Gruppi_OnShow ()
	Gruppi_Update ();
end

local function BottoneGruppi_OnClick (self)
	ChatGruppi:Debug (self:GetID ());
	gruppoCorrente = dbGruppiCopia[self:GetID ()];

	Gruppi_Update ();
	caricaMessaggi ();
end

local function BottoneEliminaGruppi_OnClick (self)
	local nome = dbGruppiCopia[self:GetID ()];
	if (gruppoCorrente == dbGruppiCopia[self:GetID ()]) then
		gruppoCorrente = dbGruppiCopia[self:GetID () -1];
	end

	table.remove (dbGruppi, find (dbGruppi, dbGruppiCopia[self:GetID ()]));
	table.remove (dbGruppiCopia, self:GetID ());
	gruppi[nome] = nil;

	gui_OnShow ();
end
---------------------------------------------------------------------------------------
-- Fine Funzioni Supporto Frame
---------------------------------------------------------------------------------------

local function round (num)
	return math.floor (num + 0.5);
end

--Creo contenuto scrollFrame gruppi
local function creaContenutoGruppi (self)
	local buttonHeight = 25;
     
    local buttonName = self:GetName() .. "_bottone_";

	local skin = skinner:GetSkin ();

	self.buttons = {};
	self.bottoniElimina = {};
	numButtons = math.ceil (self:GetHeight () / buttonHeight) - 1;

	for i = 1, numButtons do
		self.buttons[i] = CreateFrame ("BUTTON", nil, self);
		self.buttons[i]:SetSize (self:GetWidth () - 16 - 29, 25)
		self.buttons[i]:SetPoint ("TOPLEFT", self, "TOPLEFT", 8, (i - 1) * -25 - 8);
		self.buttons[i]:SetHighlightTexture ("Interface\\QuestFrame\\UI-QuestLogTitleHighlight");

		self.buttons[i]:SetNormalFontObject ("GameFontNormal");
		self.buttons[i]:SetDisabledFontObject ("GameFontDisable");
		self.buttons[i]:SetScript ("OnClick", BottoneGruppi_OnClick);

		self.buttons[i]:SetBackdrop ({
			bgFile = skin.backdrop.gruppo.sfondo,
			edgeFile = skin.backdrop.gruppo.bordi,
			tile = false,
			tileSize = 0,
			edgeSize = skin.backdrop.gruppo.spessoreBordi or 32, 
			insets = skin.backdrop.gruppo.insets or {
				left = 0,
				right = 0,
				top = 0,
				bottom = 0
			}
		});

		self.bottoniElimina[i] = CreateFrame ("BUTTON", nil, self);
		self.bottoniElimina[i]:SetSize (25, 25)
		self.bottoniElimina[i]:SetPoint ("TOPLEFT", self.buttons[i], "TOPRIGHT", 2, 0);
		self.bottoniElimina[i]:SetHighlightTexture (skin.icone.elimina.normale);
		self.bottoniElimina[i]:SetNormalTexture (skin.icone.elimina.normale);
		self.bottoniElimina[i]:SetPushedTexture (skin.icone.elimina.premuto);
		self.bottoniElimina[i]:SetDisabledTexture (skin.icone.elimina.disabilitato);
		self.bottoniElimina[i]:SetScript ("OnClick", BottoneEliminaGruppi_OnClick);

		--self.bottoniElimina[i]:SetScript ("OnClick", BottoneGruppi_OnClick);
	end

	self.buttonHeight = buttonHeight;
end

--Creo interfaccia chat
local function creaGui ()
	local skin = skinner:GetSkin ();

	--Impostazioni iniziali gui
	gui = CreateFrame ("Frame", "gxchat_frame_chat_gruppi", UIParent);
	gui:SetClampedToScreen (true);
    gui:SetFrameStrata ("DIALOG");
    gui:SetMovable (true);
    gui:SetToplevel (true);
    gui:SetWidth (584);
    gui:SetHeight (256);
    gui:EnableMouse (true);
    gui:SetPoint ("CENTER", "UIParent", "CENTER", 0, 0);
    gui:RegisterForDrag ("LeftButton");
	gui:SetScript ("OnDragStart", gui_OnDragStart);
    gui:SetScript ("OnDragStop", gui_OnDragStop);
	gui:SetScript ("OnShow", gui_OnShow);

	gui:SetBackdrop ({
		bgFile = skin.backdrop.frame.sfondo,
		edgeFile = skin.backdrop.frame.bordi,
		tile = false,
		tileSize = 0,
		edgeSize = skin.backdrop.frame.spessoreBordi or 32, 
		insets = skin.backdrop.frame.insets or {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0
		}
	});

	--Barra Titolo
	local titolo = CreateFrame ("Frame", "gxchat_frame_chat_gruppi_titolo", gui);
	titolo:SetPoint ("TOPLEFT", gui, "TOPLEFT", 0, 0);
	titolo:SetPoint ("BOTTOMRIGHT", gui, "TOPRIGHT", 0, -44);
	titolo:SetBackdrop ({
		bgFile = skin.backdrop.titolo.sfondo,
		edgeFile = skin.backdrop.titolo.bordi,
		tile = false,
		tileSize = 0,
		edgeSize = skin.backdrop.titolo.spessoreBordi or 32, 
		insets = skin.backdrop.titolo.insets or {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0
		}
	});
	gui.barraTitolo = titolo;

	local testoTitolo = gui.barraTitolo:CreateFontString (nil, nil, "GameFontNormal");
	testoTitolo:SetPoint ("TOP", titolo, "TOP", 5, -15);
	testoTitolo:SetTextColor (unpack (skin.backdrop.titolo.coloreTesto));
	testoTitolo:SetText (string.format (L["ChatGruppi_titolo"], gxchat.versione));

	local bottoneChiudi = CreateFrame ("Button", nil, titolo);
	bottoneChiudi:SetWidth (28);
	bottoneChiudi:SetHeight (28);
	bottoneChiudi:SetPoint ("TOPRIGHT", titolo, "TOPRIGHT", -10, -8);
	bottoneChiudi:SetNormalTexture (skin.icone.chiudi.normale);
	bottoneChiudi:SetPushedTexture (skin.icone.chiudi.premuto);
	bottoneChiudi:SetHighlightTexture (skin.icone.chiudi.evidenziato);
	bottoneChiudi:SetScript ("OnClick", BottoneChiudi_OnClick);
	gui.bottoneChiudi = bottoneChiudi;

	local textBox = CreateFrame ("EditBox", "gxchat_frame_chat_gruppi_textbox", gui);
	textBox:SetAutoFocus (false);
	textBox:SetHistoryLines (32);
	textBox:SetMaxLetters (255);
	textBox:SetAltArrowKeyMode (true);
	textBox:EnableMouse (true);
	textBox:SetPoint ("BOTTOMLEFT", gui, "BOTTOMLEFT", 150, 10);
	textBox:SetPoint ("BOTTOMRIGHT", gui, "BOTTOMRIGHT", -10, 10);
	textBox:SetHeight (40);
	textBox:SetFont (libSM:Fetch ("font", db.messaggio.font), db.messaggio.fontSize);
	textBox:SetTextInsets (15, 13, 0, 0);
	textBox:SetBackdrop ({
		bgFile = skin.backdrop.textBox.sfondo,
		edgeFile = skin.backdrop.textBox.bordi,
		tile = false,
		tileSize = 0,
		edgeSize = skin.backdrop.textBox.spessoreBordi or 32, 
		insets = skin.backdrop.textBox.insets or {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0
		}
	});
	textBox:SetScript ("OnEnterPressed", gui_TextBox_OnEnterPressed);
    textBox:SetScript ("OnEscapePressed", gui_TextBox_OnEscapePressed);

	gui.textBox = textBox;

	--frame messaggio
	local scrollingMessage = CreateFrame ("ScrollingMessageFrame", "gxchat_frame_chat_gruppi_scrollingMessage", gui);
	--Ancoraggio (da qui derviano le dimensioni)
	scrollingMessage:SetPoint ("TOPLEFT", gui.barraTitolo, "BOTTOMLEFT", 150, 0);
	scrollingMessage:SetPoint ("BOTTOMRIGHT", gui.textBox, "TOPRIGHT", -40, 5);
	scrollingMessage:SetHyperlinksEnabled (true);
	scrollingMessage:SetFading (false); --Non sparisce dopo che è stato scritto
	scrollingMessage:SetTextColor (db.messaggio.coloreMessaggio.r, db.messaggio.coloreMessaggio.g, db.messaggio.coloreMessaggio.b, db.messaggio.coloreMessaggio.a);
	scrollingMessage:SetMaxLines (200);
	scrollingMessage:SetJustifyH ("LEFT");
	scrollingMessage:SetInsertMode ("BOTTOM");
	scrollingMessage:Clear ();
	scrollingMessage:SetResizable (true);
	scrollingMessage:SetFont (libSM:Fetch ("font", db.messaggio.font), db.messaggio.fontSize);
	scrollingMessage:SetScript ("OnHyperlinkClick", Messaggi_OnHyperlinkClick);
	scrollingMessage:SetScript ("OnMouseWheel", Messaggi_OnMouseWheel);
	scrollingMessage:SetIndentedWordWrap (true);
	scrollingMessage:SetBackdrop ({
		bgFile = skin.backdrop.messaggi.sfondo,
		edgeFile = skin.backdrop.messaggi.bordi,
		tile = false,
		tileSize = 0,
		edgeSize = skin.backdrop.messaggi.spessoreBordi or 32, 
		insets = skin.backdrop.messaggi.insets or {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0
		}
	});

	gui.msgFrame = scrollingMessage;

	local bottoneScrollUpGruppi = CreateFrame ("Button", "gxchat_frame_chat_gruppi_bottone_scroll_up_gruppi", gui);
	bottoneScrollUpGruppi:SetWidth (32);
	bottoneScrollUpGruppi:SetHeight (32);
	bottoneScrollUpGruppi:SetPoint ("TOPLEFT", gui.barraTitolo, "BOTTOMLEFT", 12, 0);
	bottoneScrollUpGruppi:SetNormalTexture (skin.icone.scrollUp.normale);
	bottoneScrollUpGruppi:SetPushedTexture (skin.icone.scrollUp.premuto);
	bottoneScrollUpGruppi:SetHighlightTexture (skin.icone.scrollUp.evidenziato);
	bottoneScrollUpGruppi:SetDisabledTexture (skin.icone.scrollUp.disabilitato);
	bottoneScrollUpGruppi:SetScript ("OnClick", BottoneScrollUpGruppi_OnClick);
	gui.bottoneScrollUpGruppi = bottoneScrollUpGruppi;

	local bottoneScrollDownGruppi = CreateFrame ("Button", "gxchat_frame_chat_gruppi_bottone_scroll_down_gruppi", gui);
	bottoneScrollDownGruppi:SetWidth (32);
	bottoneScrollDownGruppi:SetHeight (32);
	bottoneScrollDownGruppi:SetPoint ("TOPLEFT", gui.bottoneScrollUpGruppi, "TOPRIGHT", 2, 0);
	bottoneScrollDownGruppi:SetNormalTexture (skin.icone.scrollDown.normale);
	bottoneScrollDownGruppi:SetPushedTexture (skin.icone.scrollDown.premuto);
	bottoneScrollDownGruppi:SetHighlightTexture (skin.icone.scrollDown.evidenziato);
	bottoneScrollDownGruppi:SetDisabledTexture (skin.icone.scrollDown.disabilitato);
	bottoneScrollDownGruppi:SetScript ("OnClick", BottoneScrollDownGruppi_OnClick);
	gui.bottoneScrollDownGruppi = bottoneScrollDownGruppi;

	local bottoneScrollUpMessaggi = CreateFrame ("Button", "gxchat_frame_chat_gruppi_bottone_scroll_up_messaggi", gui);
	bottoneScrollUpMessaggi:SetWidth (32);
	bottoneScrollUpMessaggi:SetHeight (32);
	bottoneScrollUpMessaggi:SetPoint ("TOPLEFT", gui.msgFrame, "TOPRIGHT", 5, 0);
	bottoneScrollUpMessaggi:SetNormalTexture (skin.icone.scrollUp.normale);
	bottoneScrollUpMessaggi:SetPushedTexture (skin.icone.scrollUp.premuto);
	bottoneScrollUpMessaggi:SetHighlightTexture (skin.icone.scrollUp.evidenziato);
	bottoneScrollUpMessaggi:SetDisabledTexture (skin.icone.scrollUp.disabilitato);
	bottoneScrollUpMessaggi:SetScript ("OnClick", BottoneScrollUpMessaggi_OnClick);
	gui.bottoneScrollUpMessaggi = bottoneScrollUpMessaggi;

	local bottoneScrollDownMessaggi = CreateFrame ("Button", "gxchat_frame_chat_gruppi_bottone_scroll_down_messaggi", gui);
	bottoneScrollDownMessaggi:SetWidth (32);
	bottoneScrollDownMessaggi:SetHeight (32);
	bottoneScrollDownMessaggi:SetPoint ("BOTTOMLEFT", gui.msgFrame, "BOTTOMRIGHT", 5, 0);
	bottoneScrollDownMessaggi:SetNormalTexture (skin.icone.scrollDown.normale);
	bottoneScrollDownMessaggi:SetPushedTexture (skin.icone.scrollDown.premuto);
	bottoneScrollDownMessaggi:SetHighlightTexture (skin.icone.scrollDown.evidenziato);
	bottoneScrollDownMessaggi:SetDisabledTexture (skin.icone.scrollDown.disabilitato);
	bottoneScrollDownMessaggi:SetScript ("OnClick", BottoneScrollDownMessaggi_OnClick);
	gui.bottoneScrollDownMessaggi = bottoneScrollDownMessaggi;

	local bottoneAggiungiGruppo = CreateFrame ("Button", "gxchat_frame_chat_gruppi_bottone_aggiungi_gruppo", gui);
	bottoneAggiungiGruppo:SetWidth (32);
	bottoneAggiungiGruppo:SetHeight (32);
	bottoneAggiungiGruppo:SetPoint ("TOPLEFT", gui.bottoneScrollDownGruppi, "TOPRIGHT", 2, 0);
	bottoneAggiungiGruppo:SetNormalTexture (skin.icone.aggiungiGruppo.normale);
	bottoneAggiungiGruppo:SetPushedTexture (skin.icone.aggiungiGruppo.premuto);
	bottoneAggiungiGruppo:SetHighlightTexture (skin.icone.aggiungiGruppo.evidenziato);
	bottoneAggiungiGruppo:SetDisabledTexture (skin.icone.aggiungiGruppo.disabilitato);
	bottoneAggiungiGruppo:SetScript ("OnClick", BottoneAggiungiGruppo_OnClick);
	gui.bottoneAggiungiGruppo = bottoneAggiungiGruppo;

	local frameGruppi = CreateFrame ("Frame", "gxchat_frame_chat_gruppi_scrollingGruppi", gui);
	frameGruppi:SetPoint ("TOPLEFT", gui.bottoneScrollUpGruppi, "BOTTOMLEFT", 0, 0);
	frameGruppi:SetPoint ("BOTTOMRIGHT", gui.textBox, "BOTTOMLEFT", -5, 0);
	frameGruppi:SetBackdrop ({
		bgFile = skin.backdrop.frameGruppi.sfondo,
		edgeFile = skin.backdrop.frameGruppi.bordi,
		tile = false,
		tileSize = 0,
		edgeSize = skin.backdrop.frameGruppi.spessoreBordi or 32, 
		insets = skin.backdrop.frameGruppi.insets or {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0
		}
	});
	frameGruppi.Update = Gruppi_Update;
	frameGruppi:SetScript ("OnMouseWheel", Gruppi_OnMouseWheel);
	frameGruppi:SetScript ("OnShow", Gruppi_OnShow);
	--frameGruppi:SetScript ("OnVerticalScroll", Gruppi_OnVerticalScroll);
	gui.frameGruppi = frameGruppi;

	creaContenutoGruppi (frameGruppi);
	frameGruppi.Update ();

	tinsert (UISpecialFrames, "gxchat_frame_chat_gruppi");
end

--Chiamata ogni volta che viene cambiato profilo impostazioni
local function Messaggio_DB_CHANGED ()
	if (not gxchat.db.profile.modules.ChatGruppi.inizializzato) then
		gxchat.db.profile.modules.ChatGruppi = gxchat:copiaTable (db_default);
	end

	db = gxchat.db.profile.modules.ChatGruppi;

	caricaFrame ();
end

--Chiamata ogni volta che viene cambiata skin
local function Messaggio_SKIN_CAMBIATA ()
	local skin = skinner:GetSkin ();

	gui:SetBackdrop ({
		bgFile = skin.backdrop.frame.sfondo,
		edgeFile = skin.backdrop.frame.bordi,
		tile = false,
		tileSize = 0,
		edgeSize = skin.backdrop.frame.spessoreBordi or 32, 
		insets = skin.backdrop.frame.insets or {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0
		}
	});

	gui.barraTitolo:SetBackdrop ({
		bgFile = skin.backdrop.titolo.sfondo,
		edgeFile = skin.backdrop.titolo.bordi,
		tile = false,
		tileSize = 0,
		edgeSize = skin.backdrop.titolo.spessoreBordi or 32, 
		insets = skin.backdrop.titolo.insets or {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0
		}
	});

	gui.msgFrame:SetBackdrop ({
		bgFile = skin.backdrop.messaggi.sfondo,
		edgeFile = skin.backdrop.messaggi.bordi,
		tile = false,
		tileSize = 0,
		edgeSize = skin.backdrop.messaggi.spessoreBordi or 32, 
		insets = skin.backdrop.messaggi.insets or {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0
		}
	});
	
	gui.frameGruppi:SetBackdrop ({
		bgFile = skin.backdrop.frameGruppi.sfondo,
		edgeFile = skin.backdrop.frameGruppi.bordi,
		tile = false,
		tileSize = 0,
		edgeSize = skin.backdrop.frameGruppi.spessoreBordi or 32, 
		insets = skin.backdrop.frameGruppi.insets or {
			left = 0,
			right = 0,
			top = 0,
			bottom = 0
		}
	});

	local buttons = gui.frameGruppi.buttons;
	local bottoniElimina = gui.frameGruppi.bottoniElimina;

	for i = 1, #buttons do
		buttons[i]:SetBackdrop ({
			bgFile = skin.backdrop.gruppo.sfondo,
			edgeFile = skin.backdrop.gruppo.bordi,
			tile = false,
			tileSize = 0,
			edgeSize = skin.backdrop.gruppo.spessoreBordi or 32, 
			insets = skin.backdrop.gruppo.insets or {
				left = 0,
				right = 0,
				top = 0,
				bottom = 0
			}
		});

		bottoniElimina[i]:SetHighlightTexture (skin.icone.elimina.normale);
		bottoniElimina[i]:SetNormalTexture (skin.icone.elimina.normale);
		bottoniElimina[i]:SetPushedTexture (skin.icone.elimina.premuto);
		bottoniElimina[i]:SetDisabledTexture (skin.icone.elimina.disabilitato);
	end

	gui.bottoneChiudi:SetNormalTexture (skin.icone.chiudi.normale);
	gui.bottoneChiudi:SetPushedTexture (skin.icone.chiudi.premuto);
	gui.bottoneChiudi:SetHighlightTexture (skin.icone.chiudi.evidenziato);

	gui.bottoneScrollUpGruppi:SetNormalTexture (skin.icone.scrollUp.normale);
	gui.bottoneScrollUpGruppi:SetPushedTexture (skin.icone.scrollUp.premuto);
	gui.bottoneScrollUpGruppi:SetHighlightTexture (skin.icone.scrollUp.evidenziato);
	gui.bottoneScrollUpGruppi:SetDisabledTexture (skin.icone.scrollUp.disabilitato);

	gui.bottoneScrollDownGruppi:SetNormalTexture (skin.icone.scrollDown.normale);
	gui.bottoneScrollDownGruppi:SetPushedTexture (skin.icone.scrollDown.premuto);
	gui.bottoneScrollDownGruppi:SetHighlightTexture (skin.icone.scrollDown.evidenziato);
	gui.bottoneScrollDownGruppi:SetDisabledTexture (skin.icone.scrollDown.disabilitato);

	gui.bottoneAggiungiGruppo:SetNormalTexture (skin.icone.aggiungiGruppo.normale);
	gui.bottoneAggiungiGruppo:SetPushedTexture (skin.icone.aggiungiGruppo.premuto);
	gui.bottoneAggiungiGruppo:SetHighlightTexture (skin.icone.aggiungiGruppo.evidenziato);
	gui.bottoneAggiungiGruppo:SetDisabledTexture (skin.icone.aggiungiGruppo.disabilitato);
end

local switchMessaggi =  {
	["DB_CHANGED"] = Messaggio_DB_CHANGED,
	["SKIN_CAMBIATA"] = Messaggio_SKIN_CAMBIATA
}

local function ChatGruppi_MessageCallback (message, ...)
	local func = switchMessaggi[message];
	if (func) then
		func (...);
	else
	end
end
---------------------------------------------------------------------------------------
-- Fine funzioni locali
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
-- Funzioni Modulo
---------------------------------------------------------------------------------------
--Inizializzazione Modulo
function ChatGruppi:OnInitialize ()
	if (not gxchat.db.profile.modules.ChatGruppi.inizializzato) then
		gxchat.db.profile.modules.ChatGruppi = gxchat:copiaTable (db_default);
	end
	if (not gxchat.db.faction.gruppi) then
		gxchat.db.faction.gruppi = {};
	end

	db			= gxchat.db.profile.modules.ChatGruppi;
	dbGruppi	= gxchat.db.faction.gruppi;

	dbGruppiCopia = gxchat:copiaTable (dbGruppi);

	table.insert (dbGruppiCopia, 1, CHAT_MSG_GUILD);
	table.insert (dbGruppiCopia, 2, CHAT_MSG_OFFICER);

	StaticPopupDialogs["gxchat_popup_gruppi"] = popupDialog;

	self:RegisterMessage ("DB_CHANGED", ChatGruppi_MessageCallback);
	self:RegisterMessage ("SKIN_CAMBIATA", ChatGruppi_MessageCallback);

	LibStub("AceConfig-3.0"):RegisterOptionsTable("gxchat_ChatGruppi opzioni", opzioni);
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("gxchat_ChatGruppi opzioni", L["gxchat_opzioni_ChatGruppi_titolo_Chat"], "gxchat");
	gxchat:registraTabellaOpzioni ("ChatGruppi", "gxchat_ChatGruppi opzioni");
end

--Chiamata quando il modulo viene abilitato
function ChatGruppi:OnEnable ()
	playerName = UnitName ("player");
	_, classe = UnitClass ("player");
	creaGui ();
	caricaFrame ();
	--Inizializzazione tabella gruppi
	gruppi[CHAT_MSG_GUILD] = gxchat:copiaTable (gruppo_default);
	gruppi[CHAT_MSG_OFFICER] = gxchat:copiaTable (gruppo_default);

	for i = 1, #dbGruppi do
		gruppi[dbGruppi[i]] = gxchat:copiaTable (gruppo_default);
	end
end

--Chiamata quando il modulo viene disabilitato
function ChatGruppi:OnDisable ()
end

function ChatGruppi:getGruppi ()
	return dbGruppiCopia;
end

function ChatGruppi:Apri (gruppo)
	gruppoCorrente = gruppo;
	if (not gui:IsShown ()) then
		gui:Show ();
	else
		gui_OnShow ();
	end
end

function ChatGruppi:AggiornaImpostazioni (versioneImpostazioni)
	self:Debug ("versioneImpostazioni = " .. versioneImpostazioni);
	--Versione 0.2.5
	if (versioneImpostazioni < 2005) then
		db.skin = "Alliance";
	end
	--Versione 0.2.40
	if (versioneImpostazioni < 2040) then
		db.skin = nil;
		db.frame = gxchat:copiaTable (db_default.frame);
	end
	--Versione 0.2.48
	if (versioneImpostazioni < 2048) then
		db.messaggio = gxchat:copiaTable (db_default.messaggio);
	end
	--Versione 0.2.147
	if (versioneImpostazioni < 2147) then
		db.messaggio.formato = {
			emoticons = false,
			stile_emoticons = ""
		}
	end
	--Versione 0.2.151
	if (versioneImpostazioni < 2151) then
		db.messaggio.formato.data = "%H:%M";
		db.messaggio.formato.server = true;
	end
	--Versione 0.2.183
	if (versioneImpostazioni < 2183) then
		db.audio = {
			attivo = true,
			suono = "[gxchat] Campanello"
		}
	end
	--Possibile bug data
	if (versioneImpostazioni < 100001 and db.messaggio.formato.data == "hh:mm") then
		db.messaggio.formato.data = "%H:%M"
	end
end

--Presente in tutti i moduli per aggiungere una impostazione al sottomenu associato
function ChatGruppi:AggiungiImpostazione (gruppo, tabImpostazione)
	self:Debug ("Entro in AggiungiImpostazione, gruppo: " .. gruppo);
	if (not gruppo or gruppo == "") then
		for k, v in pairs (tabImpostazione) do
			opzioni.args[k] = v;
		end
	else
		local tab = opzioni.args[gruppo];
		if (not tab) then
			error ("Il gruppo non esiste");
		else
			for k, v in pairs (tabImpostazione) do
				tab.args[k] = v;
			end
		end
	end

	local AceConfigRegistry = LibStub("AceConfigRegistry-3.0")
	AceConfigRegistry:NotifyChange ("gxchat_ChatGruppi opzioni");
end

--Aggiunge un messaggio al gruppo
function ChatGruppi:AggiungiMessaggio (gruppo, msg)
	ChatGruppi:Debug ("Arrivato Messaggio su " .. gruppo);
	local grp = gruppi[gruppo];
	if (not grp) then
		return;
	end
	if (grp.offset ~= 0) then
		grp.offset = grp.offset + 1;
	end

	grp.idUltimoMessaggio = grp.idUltimoMessaggio + 1;
	grp.messaggi[grp.idUltimoMessaggio] = {
		sender = msg.player,
		classe = msg.classe,
		messaggio = msg.messaggio,
		time = time ()
	}

	if (db.audio.attivo) then
		PlaySoundFile (libSharedMedia:Fetch ("sound", db.audio.suono));
	end

	if (gruppoCorrente == gruppo) then
		local msg = grp.messaggi[grp.idUltimoMessaggio];
		local mess = msg.messaggio;
		if (db.messaggio.formato.emoticons) then
			mess = libEmoticons:FormatMessage (mess, db.messaggio.formato.stile_emoticons);
		end
		local msgDaStampare = date (db.messaggio.formato.data, msg.time) .. " [" .. GetColoredName (msg.sender, msg.classe) .. "] " .. mess;

		gui.msgFrame:AddMessage (msgDaStampare);
	else
		grp.daLeggere = grp.daLeggere + 1;
		gxchat:GetModule ("BottoneMinimappa"):NuovoMessaggio ();
	end
end

--Aggiunge un messaggio alla chat di gilda
function ChatGruppi:AggiungiMessaggioGilda (msg, tipoMessaggio)
	ChatGruppi:Debug ("Entro in  AggiungiMessaggioGilda, gruppo corrente: " .. gruppoCorrente);
	local grp = gruppi[CHAT_MSG_GUILD];
	if (grp.offset ~= 0) then
		grp.offset = grp.offset + 1;
	end

	grp.idUltimoMessaggio = grp.idUltimoMessaggio + 1;
	grp.messaggi[grp.idUltimoMessaggio] = {
		sender = msg.player,
		classe = msg.classe,
		messaggio = msg.messaggio,
		time = time (),
		gilda = msg.gilda,
		server = msg.server;
		tipo = tipoMessaggio
	}

	if (db.audio.attivo) then
		PlaySoundFile (libSharedMedia:Fetch ("sound", db.audio.suono));
	end

	if (gruppoCorrente == CHAT_MSG_GUILD) then
		local msg = grp.messaggi[grp.idUltimoMessaggio];
		local mess = msg.messaggio;
		local msgDaStampare = "";
		if (msg.tipo == "msg") then
			if (db.messaggio.formato.emoticons) then
				mess = libEmoticons:FormatMessage (mess, db.messaggio.formato.stile_emoticons);
			end
			msgDaStampare = date (db.messaggio.formato.data, msg.time) .. " [" .. msg.gilda .. (db.messaggio.formato.server and ("@" .. msg.server)) .. "]" .. " [" .. GetColoredName (msg.sender, msg.classe) .. "] " .. mess;
		else
			msgDaStampare = date (db.messaggio.formato.data, msg.time) .. " [" .. msg.gilda .. (db.messaggio.formato.server and ("@" .. msg.server)) .. "]" .. string.format (msg.messaggio, msg.sender);
		end

		gui.msgFrame:AddMessage (msgDaStampare);
	end
end

--Aggiunge un messaggio alla chat di gilda
function ChatGruppi:AggiungiMessaggioOfficer (msg)
	ChatGruppi:Debug ("Arrivato messaggio officer");
	local grp = gruppi[CHAT_MSG_OFFICER];
	if (grp.offset ~= 0) then
		grp.offset = grp.offset + 1;
	end

	grp.idUltimoMessaggio = grp.idUltimoMessaggio + 1;
	grp.messaggi[grp.idUltimoMessaggio] = {
		sender = msg.player,
		classe = msg.classe,
		messaggio = msg.messaggio,
		time = time (),
		gilda = msg.gilda,
		server = msg.server;
	}

	if (db.audio.attivo) then
		PlaySoundFile (libSharedMedia:Fetch ("sound", db.audio.suono));
	end

	if (gruppoCorrente == CHAT_MSG_OFFICER) then
		local msg = grp.messaggi[grp.idUltimoMessaggio];
		local mess = msg.messaggio;
		if (db.messaggio.formato.emoticons) then
			mess = libEmoticons:FormatMessage (mess, db.messaggio.formato.stile_emoticons);
		end
		local msgDaStampare = date (db.messaggio.formato.data, msg.time) .. " [" .. msg.gilda .. (db.messaggio.formato.server and ("@" .. msg.server)) .. "]" .. " [" .. GetColoredName (msg.sender, msg.classe) .. "] " .. mess;

		gui.msgFrame:AddMessage (msgDaStampare);
	end
end
---------------------------------------------------------------------------------------
-- Fine Funzioni Modulo
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
-- Get/Set impostazioni
---------------------------------------------------------------------------------------
function ChatGruppi:SetFont (self, key)
	db.messaggio.font = key;

	gui.msgFrame:SetFont (libSM:Fetch ("font", db.messaggio.font), db.messaggio.fontSize);
	caricaMessaggi ();
end

function ChatGruppi:GetFont ()
	return db.messaggio.font;
end

function ChatGruppi:SetFontSize (self, value)
	db.messaggio.fontSize = value;

	gui.msgFrame:SetFont (libSM:Fetch ("font", db.messaggio.font), db.messaggio.fontSize);
	caricaMessaggi ();
end

function ChatGruppi:GetFontSize ()
	return db.messaggio.fontSize;
end

function ChatGruppi:GetFontColor ()
	return db.messaggio.coloreMessaggio.r, db.messaggio.coloreMessaggio.g, db.messaggio.coloreMessaggio.b, db.messaggio.coloreMessaggio.a;
end

function ChatGruppi:SetFontColor (info, r, g, b, a)
	if (r and g and b and a) then
		db.messaggio.coloreMessaggio.r, db.messaggio.coloreMessaggio.g, db.messaggio.coloreMessaggio.b, db.messaggio.coloreMessaggio.a = r, g, b, a;
		gui.msgFrame:SetTextColor (db.messaggio.coloreMessaggio.r, db.messaggio.coloreMessaggio.g, db.messaggio.coloreMessaggio.b, db.messaggio.coloreMessaggio.a);
		caricaMessaggi ();
	end
end

function ChatGruppi:GetEmoticons ()
	return db.messaggio.formato.emoticons;
end

function ChatGruppi:SetEmoticons (info, abilitato)
	db.messaggio.formato.emoticons = abilitato;
	caricaMessaggi ();
end

function ChatGruppi:GetStyle ()
	return db.messaggio.formato.stile_emoticons;
end

function ChatGruppi:SetStyle (info, style)
	db.messaggio.formato.stile_emoticons = style;
	caricaMessaggi ();
end

function ChatGruppi:SetFormatoData (info, formato)
	db.messaggio.formato.data = formato;
	caricaMessaggi ();
end

function ChatGruppi:GetFormatoData ()
	return db.messaggio.formato.data;
end

function ChatGruppi:SetServer (info, abilitato)
	db.messaggio.formato.server = abilitato;
end

function ChatGruppi:GetServer ()
	return db.messaggio.formato.server;
end

function ChatGruppi:GetAudioAttivo ()
	return db.audio.attivo;
end

function ChatGruppi:SetAudioAttivo (info, abilitato)
	db.audio.attivo = abilitato;
end

function ChatGruppi:GetSuono ()
	return db.audio.suono;
end

function ChatGruppi:SetSuono (info, suono)
	db.audio.suono = suono;
end
---------------------------------------------------------------------------------------
-- Fine Get/Set impostazioni
---------------------------------------------------------------------------------------