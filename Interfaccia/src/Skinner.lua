-- Author      : Galilman
-- Create Date : 23/07/2015 14:20:02

local gxchat = gxchat;
local Skinner = gxchat:NewModule("Skinner");

local L = LibStub("AceLocale-3.0"):GetLocale("gxchat");

local tabSkin = {};

local db_default = {
	inizializzato = true,
	abilitato = true,
	skin = "Alliance"
}

local db;

local function preparaTab ()
	local tab = {};
	for k, v in pairs (tabSkin) do
		tab[k] = k;
	end
	return tab;
end

local function setSkin (skin)
	db.skin = skin;
	Skinner:SendMessage ("SKIN_CAMBIATA");
end

local function getSkin ()
	return db.skin;
end

local tabOpzioni = {
	skin = {
		name = L["opzioni_Skinner_Skin"],
		type = "select",
		values = preparaTab,
		get = getSkin,
		set = setSkin
	}
};

local function DB_CAMBIATO ()
	if (not gxchat.db.profile.modules.Skinner.inizializzato) then
		gxchat.db.profile.modules.Skinner = gxchat:copiaTable (db_default);
	end

	db = gxchat.db.profile.modules.Skinner;

	Skinner:SendMessage ("SKIN_CAMBIATA");
end

---------------------------------------------------------------------------------------
-- Funzioni Modulo
---------------------------------------------------------------------------------------
--Inizializzazione Modulo
function Skinner:OnInitialize ()
	if (not gxchat.db.profile.modules.Skinner.inizializzato) then
		gxchat.db.profile.modules.Skinner = gxchat:copiaTable (db_default);
	end

	db = gxchat.db.profile.modules.Skinner;

	self:RegisterMessage ("DB_CHANGED", DB_CAMBIATO);
end

--Chiamata quando il modulo viene abilitato
function Skinner:OnEnable ()
	Skinner:Debug ("test 12345");
	gxchat:aggiungiOpzione ("gxchat_ChatGruppi opzioni", "gruppoAudioVideo", tabOpzioni);
end

--Chiamata quando il modulo viene disabilitato
function Skinner:OnDisable ()
end

--Aggiunge una skin al modulo
function Skinner:AggiungiSkin (nome, skin)
	tabSkin[nome] = skin;
end

--Recupero la skin
function Skinner:GetSkin ()
	return tabSkin[db.skin] or tabSkin[db_default.skin];
end

---------------------------------------------------------------------------------------
-- Fine Funzioni Modulo
---------------------------------------------------------------------------------------