==== 1.0.1:
===== EN
* Fix bug with wow 6.2.4
===== IT
* Fix bug con wow 6.2.4

==== 0.2.0:
===== EN
* Added Officer Chat
===== IT
* Aggiunta chat officer

==== 0.2.0:
===== EN
* Added to WoW chat
* Started implementation of chat groups
===== IT
* Integrato in chat di WoW
* Iniziata implementazione gruppi chat

==== 0.1.2:
===== EN
* Added LibEmoticons for chat emoticons (you need to install a emoticons pack)
* Added sound notification
===== IT
* Aggiunta LibEmoticons per emoticons in chat, per utilizzarle bisogna scaricare un pacchetto di emoticons
* Aggiunto avviso sonoro in ricezione messaggi

==== 0.1.1 beta libreria:
* Spostata parte di comunicazione in libreria
==== 0.1.1 alpha v4:
* Aggiunto supporto Emoticons
==== 0.1.1 alpha v3:
* Cambiato codice invio su battle.net per risolvere bug player connessi contemporaneamente dal client e da wow