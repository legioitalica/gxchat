gxchat = LibStub("AceAddon-3.0"):NewAddon("gxchat", "AceConsole-3.0", "AceEvent-3.0");

--Imposto le librerie di default dei moduli (AceConsole per Print e AceEvent in quanto richiesta da tutti i moduli)
gxchat:SetDefaultModuleLibraries ("AceConsole-3.0", "AceEvent-3.0");

--Localizzazione
local L = LibStub("AceLocale-3.0"):GetLocale("gxchat");

--Tabella con i moduli predefiniti
gxchat.prototipiModuli = {
	gui = {
		--db = gxchat.db.profile,
		disattivabile = false,
		Mostra = function ()
		end,
		Nascondi = function ()
		end,
		AggiornaImpostazioni = function (versioneImpostazioni)
		end,
		AggiungiImpostazione = function (gruppo, tabImpostazione)
		end,
		Debug = function (self, messaggio)
			if (gxchat:IsDebug ()) then
				self:Print (messaggio);
			end
		end
	},
	modulo = {
		AggiornaImpostazioni = function (versioneImpostazioni)
		end,
		AggiungiImpostazione = function (gruppo, tabImpostazione)
		end,
		Debug = function (self, messaggio)
			if (gxchat:IsDebug ()) then
				self:Print (messaggio);
			end
		end,
		Dump = function ()
		end
	}
}
gxchat:SetDefaultModulePrototype (gxchat.prototipiModuli.modulo);

local defaults = {
	profile = {
		gxchat = {
			debug = false,
			creato = "",
			versioneImpostazioni = 0
		},
		modules = {
			["*"] = {
				abilitato = false,
				inizializzato = false
			}
		}
	},
	faction = {
	}
}

local opzioni = {
	name = "gxchat",
	type = "group",
	handler = gxchat,
	args = {
		debug = {
			name = "Debug",
			desc = L["opzione_debug"];
			type = "toggle",
			get = "IsDebug",
			set = "SetDebug"
		}
	}
}

local function registraFont ()
	local LSM3 = LibStub("LibSharedMedia-3.0", true);

	LSM3:Register ("font", "Bernard MT Condensed", "Interface\\Addons\\gxchat\\Fonts\\bernard mt condensed.ttf");
	LSM3:Register ("font", "FlorinaUT BoldItalic", "Interface\\Addons\\gxchat\\Fonts\\florinaut bolditalic.ttf");
end

local function registraAudio ()
	local LSM3 = LibStub("LibSharedMedia-3.0", true);

	LSM3:Register ("sound", "[gxchat] Campanello", "Interface\\Addons\\gxchat\\Audio\\Campanello.ogg");
	LSM3:Register ("sound", "[One Piece] Super Franky", "Interface\\Addons\\gxchat\\Audio\\Suuuuuperrr!!!.ogg");
	LSM3:Register ("sound", "[Hearthstone] Sylvanas Attack", "Interface\\Addons\\gxchat\\Audio\\Sylvanas.ogg");
end

local function stampaHelp ()
	print (L["gxchat_help"]);
	print (L["gxchat_help_debug"]);
	print (L["gxchat_help_settings"]);
	print (L["gxchat_help_checkMembers"]);
	print (L["gxchat_help_checkVersion"]);
	print (L["gxchat_help_refresh"]);
	print (L["gxchat_help_help"]);
end

function gxchat:OnInitialize()
	self.db = LibStub("AceDB-3.0"):New("gxchat_data", defaults);
	if (self.db.profile.gxchat.creato == "") then
		self.db.profile.gxchat.creato = date ();
	end

	self.gildeAssociate = nil;

	--Registro comandi addon
	self:RegisterChatCommand ("gxchat", "ChatCommand")

	opzioni.args.profili = LibStub("AceDBOptions-3.0"):GetOptionsTable (self.db);
	LibStub("AceConfig-3.0"):RegisterOptionsTable("gxchat opzioni", opzioni);
	self.frameOpzioni = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("gxchat opzioni", "gxchat");

	self.db.RegisterCallback (self, "OnProfileChanged", "RefreshConfig");
	self.db.RegisterCallback (self, "OnProfileCopied", "RefreshConfig");
	self.db.RegisterCallback (self, "OnProfileReset", "RefreshConfig");

	self.versione = tostring (GetAddOnMetadata ("gxchat", "Version")) or "0.0.0"
end

function gxchat:OnEnable()
	gxchat:controllaImpostazioni ();

	--Abilita i moduli attivi
	for nome, module in self:IterateModules() do
		if (self.db.profile.modules[nome].abilitato) then
			module:Enable ();
		else
			module:Disable ();
		end
	end

	--Registro i font e l'audio
	registraFont ();
	registraAudio ();
end

function gxchat:OnDisable()
		-- Called when the addon is disabled
end

function gxchat:ChatCommand (input)
	if (not input or input == "") then
		--InterfaceOptionsFrame_OpenToCategory ("gxchat");
		--InterfaceOptionsFrame_OpenToCategory ("gxchat");
		self:GetModule ("ChatGruppi"):Apri (CHAT_MSG_GUILD);
	else
		local args = self:split (input, "-");
		for i = 1, #args do
			args[i] = self:trim (args[i]);
			if string.find (args[i], "debug") then
				local split = self:split (args[i], "=")
				if table.getn(split) == 1 then
					self:Print (L["msg_debug"] .. (self:IsDebug() and L["debug_abilitato"] or L["debug_disabilitato"]))
				elseif split[2] == "on" then
					self:SetDebug (nil, true)
				elseif split[2] == "off" then
					self:SetDebug (nil, false)
				end
			elseif (string.find (args[i], "settings") or string.find (args[i], L["cmd_impostazioni"])) then
				local split = self:split (args[i], "=")
				if table.getn(split) == 1 then
					InterfaceOptionsFrame_OpenToCategory ("gxchat");
					InterfaceOptionsFrame_OpenToCategory ("gxchat");
				elseif (split[2] == "guild" or split[2] == L["cmd_impostazioni_gilda"]) then
					self:SendMessage ("GESTIONE_GILDE_OPEN_FRAME");
				end
			elseif (args[i] == "checkMembers" or args[i] == L["cmd_controllo_membri"]) then
				self:SendMessage ("CONTROLLO_MEMBRI");
			elseif (args[i] == "checkVersion") then
				self:GetModule ("ControlloVersione"):EffettuaControllo ();
			elseif (args[i] == "refresh") then
				self:SendMessage ("GILDE_CAMBIATE");
			elseif (args[i] == "h" or args[i] == "help") then
				stampaHelp ();
			elseif (args[i] == "dump") then
				for name, module in self:IterateModules() do
					module:Dump ();
				end
			end
		end
	end
end

--[[
funzioni di utility
]]
function gxchat:IsDebug ()
	return self.db.profile.gxchat.debug;
end

function gxchat:SetDebug (info, attivo)
	self.db.profile.gxchat.debug = attivo;

	self:Print (L["msg_debug"] .. (self:IsDebug() and L["debug_abilitato"] or L["debug_disabilitato"]))
end

function gxchat:split(str, sep)
	if sep == nil then
		sep = "%s"
	end
    
    local t={} 
	local i=1

	for str in string.gmatch(str, "([^" .. sep .. "]+)") do
		t[i] = str
        i = i + 1
    end

    return t
end

function gxchat:copiaTable (t)
	if (type(t) ~= "table") then
		return t;
	end
	local mt = getmetatable (t);
	local res = {};
	for k, v in pairs(t) do
		if (type(v) == "table") then
			v = self:copiaTable (v);
		end
		res[k] = v;
	end
	setmetatable (res, mt);
	return res;
end

function gxchat:trim(s)
	return (s:gsub("^%s*(.-)%s*$", "%1"))
end

function gxchat:RefreshConfig ()
	if (not self.db.profile.gxchat) then
		self.db.profile = self:copiaTable (defaults.profile);
		if (self.db.profile.gxchat.creato == "") then
			self.db.profile.gxchat.creato = date ();
		end
	end
	gxchat:controllaImpostazioni ();
	self:SendMessage ("DB_CHANGED");
end

function gxchat:literalize(str)
    return str:gsub("[%(%)%.%%%+%-%*%?%[%]%^%$]", "%%%0")
end

function gxchat:controllaImpostazioni ()
	--Controllo le impostazioni e aggiorno se nuova versione
	local versioneLocale = gxchat:split (gxchat.versione, ".");
	versioneLocale = tonumber (versioneLocale[1]) * 100000 + tonumber (versioneLocale[2]) * 1000 + tonumber(versioneLocale[3]);

	if (not self.db.profile.gxchat.versioneImpostazioni or self.db.profile.gxchat.versioneImpostazioni < versioneLocale) then
		for name, module in self:IterateModules() do
			if (self:IsDebug ()) then
				self:Print ("aggiorno impostazioni di " .. name);
			end
			module:AggiornaImpostazioni (self.db.profile.gxchat.versioneImpostazioni or 0);
		end

		if (self.db.profile.gxchat.versioneImpostazioni < 2183) then
			self.db.profile.modules["InterfacciaChat"] = nil;
		end

		self.db.profile.gxchat.versioneImpostazioni = versioneLocale;
	end
end

local tabOpzioni = {};

function gxchat:registraTabellaOpzioni (modulo, pagina)
	if (self:IsDebug ()) then
		self:Print ("Registro Opzioni per " .. modulo .. " in pagina " .. pagina);
	end
	if (type (modulo) ~= "string" and type (pagina) ~= "string") then
		error ("modulo e tabella devono essere string");
	end
	tabOpzioni[pagina] = modulo;
end

function gxchat:aggiungiOpzione (pagina, gruppo, tabImpostazione)
	if (self:IsDebug ()) then
		self:Print ("Aggiungo opzioni su pagina " .. pagina .. " in gruppo " .. gruppo);
	end
	if (pagina == "gxchat") then
		if (not gruppo or gruppo == "") then
			for k, v in pairs (tabImpostazione) do
				opzioni.args[k] = v;
			end
		else
			local tab = opzioni.args[gruppo];
			if (not tab) then
				error ("Il gruppo non esiste");
			else
				for k, v in pairs (tabImpostazione) do
					tab[k] = v;
				end
			end
		end
		local AceConfigRegistry = LibStub("AceConfigRegistry-3.0")
		AceConfigRegistry:NotifyChange ("gxchat opzioni");
	else
		local modulo = tabOpzioni[pagina];
		local modulo = gxchat:GetModule (modulo);
		local stato = modulo:IsEnabled ();

		if (not stato) then
			modulo:Enable ();
		end

		modulo:AggiungiImpostazione (gruppo, tabImpostazione);

		if (not stato) then
			modulo:Disable ();
		end
	end
end

function gxchat:RegistraSkin (nome, skin)
	gxchat:GetModule ("Skinner"):AggiungiSkin(nome, skin);
end