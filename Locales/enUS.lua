-- Author      : Galilman
-- Create Date : 17/10/2014 14:59:34

local L = LibStub("AceLocale-3.0"):NewLocale("gxchat", "enUS", true)

if L then
	L["nome_addon"] = "gxchat";
	L["versione_addon_corrente"] = "Current Version: %s";
	L["info_autore"] = "Made by Legio Italica Addons team";
	L["titolo_tooltip_minimappa"] = "gxchat |cff20ff20 %s |r";
	L["tooltip_minimappa_click_sinistro"] = "|cffffff00 Click|r to show the chat window";
	L["tooltip_minimappa_click_destro"] = "|cffffff00 Click right|r to show the options";
	L["tooltip_minimappa_trascina"] = "|cffffff00 Drag|r for move around the map";
	L["tooltip_minimappa_shift_trascina"] = "|cffffff00 Hold shift and drag|r for drag free around the map";
	L["opzioni_minimappa_blocca_bottone"] = "Block button movement";
	L["opzioni_minimappa_mostra_icona"] = "Show minimap icon";
	L["opzioni_minimappa_apri_opzioni"] = "Open the options";
	L["msg_debug"] = "Debug status: "
	L["msg_blocca"] = "Frame locked";
	L["msg_sblocca"] = "Frame unlocked";
	L["debug_abilitato"] = "Enabled"
	L["debug_disabilitato"] = "Disabled" 
	L["ChatGruppi_titolo"] = "gxchat %s";
	L["gxchat_nuova_versione"] = "gxchat: A new version is available: |cff20ff20%s|r; you are using the version |cffff0033%s|r";

	---------------------------------------------------------------------------------------
	-- Parametri comando /gxchat
	---------------------------------------------------------------------------------------
	L["cmd_opzioni"] = "Options";
	L["cmd_impostazioni"] = "settings";
	L["cmd_impostazioni_gilda"] = "Guild";
	L["cmd_controllo_membri"] = "checkMembers";

	---------------------------------------------------------------------------------------
	-- Frame Errore
	---------------------------------------------------------------------------------------
	L["gxchat_error_frame_title"] = "Error occured";

	---------------------------------------------------------------------------------------
	-- Gestione Gilde
	---------------------------------------------------------------------------------------
	L["gxchat_GestioneGilde_FrameHeader"] = "Guild Amministration affiliates";
	L["gxchat_GestioneGilde_Frame_Descrizione"] = "Add or remove affiliates guild";
	L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Header"] = "Add New Guild";
	L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Nome"] = "Guild Name";
	L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Pulsante_Aggiungi"] = "Add";
	L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Server"] = "Guild Server";
	L["gxchat_GestioneGilde_Frame_Gilde"] = "Affiliates Guild list";
	L["gxchat_GestioneGilde_Frame_Salva_Modifiche"] = "Save Change";
	L["gxchat_GestioneGilde_Frame_Elimina_Gilda"] = "Delete Guild";
	-- Frame Errore
	L["gxchat_GestioneGilde_error_frame_impossibile_salvare"] = "You don't have the permission to change the guild note,for this reason you can't change the affiliates guild. Only guild officer can do it.";
	L["gxchat_GestioneGilde_error_frame_superati_caratteri_massimi"] = "Impossible save the affiliates guild, too many font on guild note; %s Used on a base of 500 font.";
	---------------------------------------------------------------------------------------
	-- Fine Gestione Gilde
	---------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------
	-- Controllo Membri
	---------------------------------------------------------------------------------------
	L["gxchat_ControlloMembri_frame_title"] = "Member control";
	---------------------------------------------------------------------------------------
	-- Fine Controllo Membri
	---------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------
	-- Help
	---------------------------------------------------------------------------------------
	L["gxchat_help"] = "gxchat %s guild chat and cross server group chat. Use: /gxchat [options]\nNo Option: Opens the window chat\nOptions:";
	L["gxchat_help_debug"]        = "  -debug[=on|off]   = Without any parameter shows the debug state. With parameter able\\disable the debug messages";
	L["gxchat_help_settings"]     = "  -settings[=guild] = Without any parameter opens the configuration window. With parameter \"guild\" opens the configuration window affiliated guild (Available only to the officers)";
	L["gxchat_help_checkMembers"] = "  -checkMembers     = Shows who uses the addon in the guild. (Command allowed only for the officers)";
	L["gxchat_help_checkVersion"] = "  -checkVersion     = Check if the version in use is the last one available";
	L["gxchat_help_refresh"]      = "  -refresh          = Read the affiliates guild from the guild notes again, helpful if messages from other guilds are not being displayed";
	L["gxchat_help_help"]         = "  -h or -help   = Show this guild";
	---------------------------------------------------------------------------------------
	-- Fine Help
	---------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------
	-- Opzioni
	---------------------------------------------------------------------------------------
	L["opzione_debug"] = "Enable and disable the Addon's message (to keep disable unless developers request)";
	--BottoneMinimappa
	L["opzioni_minimappa_visibile"] = "Show minimap icon";
	L["opzioni_minimappa_blocca"] = "Block icon";
	L["gxchat_opzioni_BottoneMinimappa_titolo_Icona_Minimappa"] = "Minimap Icon";
	--ChatGruppi
	L["opzioni_ChatGruppi_font"] = "Font of Type";
	L["opzioni_ChatGruppi_font_desc"] = "Chose chat font";
	L["opzioni_ChatGruppi_dimensione_font"] = "Font size";
	L["opzioni_ChatGruppi_colore_font"] = "Font color";
	L["opzioni_ChatGruppi_formato_header"] = "Format Message";
	L["opzioni_ChatGruppi_formato_data"] = "Show date";
	L["opzioni_ChatGruppi_font_header"] = "Font";
	L["opzioni_ChatGruppi_formato_emoticons"] = "Show emoticons on the chat window";
	L["opzioni_ChatGruppi_formato_emoticons_stile"] = "Emoticons style";
	L["opzioni_ChatGruppi_formato_emoticons_disabilitato"] = "To be able to use Emoticons, you have to download an Extra Pack";
	L["gxchat_opzioni_ChatGruppi_titolo_Chat"] = "Groups Chat";
	L["gxchat_opzioni_ChatGruppi_data"] = "Date";
	L["gxchat_opzioni_ChatGruppi_mostra_server"] = "Show server (only for guild chat and officers)";
	L["gxchat_ChatGruppi_aggiungi_gruppo_titolo"] = "New Group";
	L["gxchat_ChatGruppi_aggiungi_gruppo_aggiungi"] = "Add";
	L["gxchat_ChatGruppi_aggiungi_gruppo_annulla"] = "Cancel";
	L["opzioni_ChatGruppi_formato_anteprima"] = "Preview";
	L["opzioni_ChatGruppi_formato_anteprima_gruppi"] = "Groups";
	L["opzioni_ChatGruppi_audio_video_header"] = "Graphics and Sound";
	L["opzioni_ChatGruppi_audio_attivo"] = "Enable audio alert when a message is received";
	L["opzioni_ChatGruppi_audio_suono"] = "Audio alert";
	--InterfacciaChat
	L["opzioni_InterfacciaChat_font"] = "Font of Type";
	L["opzioni_InterfacciaChat_font_desc"] = "Choose chat font";
	L["opzioni_InterfacciaChat_dimensione_font"] = "Font size";
	L["opzioni_InterfacciaChat_colore_font"] = "Font color";
	L["opzioni_InterfacciaChat_formato_header"] = "Format Message";
	L["opzioni_InterfacciaChat_formato_data"] = "Show date";
	L["opzioni_InterfacciaChat_formato_server"] = "Show server";
	L["opzioni_InterfacciaChat_font_header"] = "Font";
	L["opzioni_InterfacciaChat_comportamento_header"] = "Module behavior";
	L["opzioni_InterfacciaChat_comportamento_in_combat"] = "Hide automatically in combat";
	L["opzioni_InterfacciaChat_comportamento_apri_automaticamente"] = "Open automatically when a message is received";
	L["opzioni_InterfacciaChat_formato_emoticons"] = "Visualizza emoticons nella finestra chat";
	L["opzioni_InterfacciaChat_formato_emoticons_stile"] = "Stile emoticons";
	L["opzioni_InterfacciaChat_formato_emoticons_disabilitato"] = "To be able to use the Emoticons, you have to download an extra Pack";
	L["gxchat_opzioni_InterfacciaChat_titolo_Chat"] = "Chat";
	L["opzioni_InterfacciaChat_comportamento_visualizza_finestra"] = "Show outer chat window";
	--RicezioneMessaggi
	L["opzioni_RicezioniMessaggi_avviso_sonoro_abilitato"] = "Play sound when a message is received";
	L["opzioni_RicezioniMessaggi_avviso_sonoro_tipo_suono"] = "Sound";
	L["opzioni_RicezioniMessaggi_chat_table_colore_classe"] = "Show class color";
	L["gxchat_opzioni_RicezioneMessaggi_titolo"] = "Chat WoW";
	L["opzioni_RicezioneMessaggi_formato"] = "Format";
	L["opzioni_RicezioneMessaggi_formato_anteprima"] = "Preview";
	L["opzioni_RicezioneMessaggi_gruppo_messaggi"] = "Message";
	L["opzioni_RicezioneMessaggi_gruppo_achievements"] = "Achievements";
	--Skinner
	L["opzioni_Skinner_Skin"] = "Skin";
	---------------------------------------------------------------------------------------
	-- Fine Opzioni
	---------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------
	-- Comunicazione
	---------------------------------------------------------------------------------------
	L["gxchat_Comunicazione_errore_registraControllo"] = "strIDRisposta (%s) already used by another module";
	---------------------------------------------------------------------------------------
	-- Fine Comunicazione
	---------------------------------------------------------------------------------------
end