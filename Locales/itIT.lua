﻿-- Author      : Galilman
-- Create Date : 09/09/2014 15:55:05

local L = LibStub("AceLocale-3.0"):NewLocale("gxchat", "itIT")

if L then
	L["nome_addon"] = "gxchat";
	L["versione_addon_corrente"] = "Versione Corrente: %s";
	L["info_autore"] = "Creato da Legio Italica addon team";
	L["titolo_tooltip_minimappa"] = "gxchat |cff20ff20 %s |r";
	L["tooltip_minimappa_click_sinistro"] = "|cffffff00 Click|r per mostrare la finestra della chat";
	L["tooltip_minimappa_click_destro"] = "|cffffff00 Click destro|r per mostrare le opzioni";
	L["tooltip_minimappa_trascina"] = "|cffffff00 Trascina|r per spostare attorno alla mappa";
	L["tooltip_minimappa_shift_trascina"] = "|cffffff00 Tieni premuto shift e trascina|r per spostare liberamente attorno alla mappa";
	L["opzioni_minimappa_blocca_bottone"] = "Blocca movimento bottone";
	L["opzioni_minimappa_mostra_icona"] = "Mostra icona minimappa";
	L["opzioni_minimappa_apri_opzioni"] = "Apri le opzioni";
	L["msg_debug"] = "Stato debug: "
	L["msg_blocca"] = "Frame Bloccato";
	L["msg_sblocca"] = "Frame Sbloccato";
	L["debug_abilitato"] = "abilitato"
	L["debug_disabilitato"] = "disabilitato" 
	L["ChatGruppi_titolo"] = "gxchat %s";
	L["gxchat_nuova_versione"] = "gxchat: È disponibile una nuova versione: |cff20ff20%s|r; tu stai usando la versione |cffff0033%s|r";

	---------------------------------------------------------------------------------------
	-- Parametri comando /gxchat
	---------------------------------------------------------------------------------------
	L["cmd_opzioni"] = "opzioni";
	L["cmd_impostazioni"] = "impostazioni";
	L["cmd_impostazioni_gilda"] = "gilda";
	L["cmd_controllo_membri"] = "controlloMembri";

	---------------------------------------------------------------------------------------
	-- Frame Errore
	---------------------------------------------------------------------------------------
	L["gxchat_error_frame_title"] = "C'è stato un errore";

	---------------------------------------------------------------------------------------
	-- Gestione Gilde
	---------------------------------------------------------------------------------------
	L["gxchat_GestioneGilde_FrameHeader"] = "Gestione Gilde Associate";
	L["gxchat_GestioneGilde_Frame_Descrizione"] = "Aggiungi o rimuovi gilde associate";
	L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Header"] = "Aggiungi Nuova Gilda";
	L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Nome"] = "Nome Gilda";
	L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Pulsante_Aggiungi"] = "Aggiungi";
	L["gxchat_GestioneGilde_Frame_Nuova_Gilda_Server"] = "Server della gilda";
	L["gxchat_GestioneGilde_Frame_Gilde"] = "Elenco Gilde Associate";
	L["gxchat_GestioneGilde_Frame_Salva_Modifiche"] = "Salva Modifiche";
	L["gxchat_GestioneGilde_Frame_Elimina_Gilda"] = "Elimina gilda";
	-- Frame Errore
	L["gxchat_GestioneGilde_error_frame_impossibile_salvare"] = "Non hai i permessi per modificare le note di gilda, per questo non puoi cambiare le gilde associate alla tua gilda. Fai fare questa operazione a un officer.";
	L["gxchat_GestioneGilde_error_frame_superati_caratteri_massimi"] = "Impossibile salvare le gilde associate, troppi caratteri nelle note di gilda; usati %s su un massimo di 500 caratteri.";
	---------------------------------------------------------------------------------------
	-- Fine Gestione Gilde
	---------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------
	-- Controllo Membri
	---------------------------------------------------------------------------------------
	L["gxchat_ControlloMembri_frame_title"] = "Controllo Membri";
	---------------------------------------------------------------------------------------
	-- Fine Controllo Membri
	---------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------
	-- Help
	---------------------------------------------------------------------------------------
	L["gxchat_help"] = "gxchat %s chat gilda e gruppi chat cross server. Uso: /gxchat [options]\nNessuna Opzione: Apre la finestra chat\nOptions:";
	L["gxchat_help_debug"]        = "  -debug[=on|off]   = Senza nessun parametro mostra lo stato del debug. Con parametro abilita\\disabilita i messaggi di debug";
	L["gxchat_help_settings"]     = "  -settings[=guild] = Senza nessun parametro apre la finestra di configurazione. Con parametro \"guild\" apre la finestra configurazione gilde associate (Disponibile solo agli officer di gilda";
	L["gxchat_help_checkMembers"] = "  -checkMembers     = Mostra chi nella gilda utilizza l'addon. (Comando disponibile solo agli officer di gilda)";
	L["gxchat_help_checkVersion"] = "  -checkVersion     = Controlla se la versione in uso è l'ultima disponibile";
	L["gxchat_help_refresh"]      = "  -refresh          = Legge nuovamente le gilde associate dalle note di gilda, utile se non vengono visualizzati messaggi da gilde esterne";
	L["gxchat_help_help"]         = "  -h oppure -help   = Mostra questa guida";
	---------------------------------------------------------------------------------------
	-- Fine Help
	---------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------
	-- Opzioni
	---------------------------------------------------------------------------------------
	L["opzione_debug"] = "Abilita o disabilita i messaggi di debug dell'Addon (da tenere disattivato salvo richiesta degli sviluppatori)";
	--BottoneMinimappa
	L["opzioni_minimappa_visibile"] = "Visualizza icona minimappa";
	L["opzioni_minimappa_blocca"] = "Blocca icona";
	L["gxchat_opzioni_BottoneMinimappa_titolo_Icona_Minimappa"] = "Icona Minimappa";
	--ChatGruppi
	L["opzioni_ChatGruppi_font"] = "Tipo Carattere";
	L["opzioni_ChatGruppi_font_desc"] = "Seleziona il font che vuoi usare per la chat";
	L["opzioni_ChatGruppi_dimensione_font"] = "Dimensione Carattere";
	L["opzioni_ChatGruppi_colore_font"] = "Colore Carattere";
	L["opzioni_ChatGruppi_formato_header"] = "Formato Messaggio";
	L["opzioni_ChatGruppi_formato_data"] = "Mostra data";
	L["opzioni_ChatGruppi_font_header"] = "Carattere";
	L["opzioni_ChatGruppi_formato_emoticons"] = "Visualizza emoticons nella finestra chat";
	L["opzioni_ChatGruppi_formato_emoticons_stile"] = "Stile emoticons";
	L["opzioni_ChatGruppi_formato_emoticons_disabilitato"] = "Per poter utilizzare le emoticons bisogna installare un pacchetto";
	L["gxchat_opzioni_ChatGruppi_titolo_Chat"] = "Chat Gruppi";
	L["gxchat_opzioni_ChatGruppi_data"] = "Data";
	L["gxchat_opzioni_ChatGruppi_mostra_server"] = "Mostra server (solo per chat gilda e officer)";
	L["gxchat_ChatGruppi_aggiungi_gruppo_titolo"] = "Nuovo Gruppo";
	L["gxchat_ChatGruppi_aggiungi_gruppo_aggiungi"] = "Aggiungi";
	L["gxchat_ChatGruppi_aggiungi_gruppo_annulla"] = "Annulla";
	L["opzioni_ChatGruppi_formato_anteprima"] = "Anteprima";
	L["opzioni_ChatGruppi_formato_anteprima_gruppi"] = "Gruppi";
	L["opzioni_ChatGruppi_audio_video_header"] = "Grafica & Suono";
	L["opzioni_ChatGruppi_audio_attivo"] = "Abilita avviso sonoro a messaggio ricevuto";
	L["opzioni_ChatGruppi_audio_suono"] = "Suono avviso";
	--InterfacciaChat
	L["opzioni_InterfacciaChat_font"] = "Tipo Carattere";
	L["opzioni_InterfacciaChat_font_desc"] = "Seleziona il font che vuoi usare per la chat";
	L["opzioni_InterfacciaChat_dimensione_font"] = "Dimensione Carattere";
	L["opzioni_InterfacciaChat_colore_font"] = "Colore Carattere";
	L["opzioni_InterfacciaChat_formato_header"] = "Formato Messaggio";
	L["opzioni_InterfacciaChat_formato_data"] = "Mostra data";
	L["opzioni_InterfacciaChat_formato_server"] = "Mostra server";
	L["opzioni_InterfacciaChat_font_header"] = "Carattere";
	L["opzioni_InterfacciaChat_comportamento_header"] = "Comportamento Modulo";
	L["opzioni_InterfacciaChat_comportamento_in_combat"] = "Nascondi automaticamente in combat";
	L["opzioni_InterfacciaChat_comportamento_apri_automaticamente"] = "Apri automaticamente quando viene ricevuto un messaggio";
	L["opzioni_InterfacciaChat_formato_emoticons"] = "Visualizza emoticons nella finestra chat";
	L["opzioni_InterfacciaChat_formato_emoticons_stile"] = "Stile emoticons";
	L["opzioni_InterfacciaChat_formato_emoticons_disabilitato"] = "Per poter utilizzare le emoticons bisogna installare un pacchetto";
	L["gxchat_opzioni_InterfacciaChat_titolo_Chat"] = "Chat";
	L["opzioni_InterfacciaChat_comportamento_visualizza_finestra"] = "Mostra finestra chat esterna";
	--RicezioneMessaggi
	L["opzioni_RicezioniMessaggi_avviso_sonoro_abilitato"] = "Abilita avviso sonoro a messaggio ricevuto";
	L["opzioni_RicezioniMessaggi_avviso_sonoro_tipo_suono"] = "Suono avviso";
	L["opzioni_RicezioniMessaggi_chat_table_colore_classe"] = "Mostra colore classe";
	L["gxchat_opzioni_RicezioneMessaggi_titolo"] = "Chat WoW";
	L["opzioni_RicezioneMessaggi_formato"] = "Formato Messaggio";
	L["opzioni_RicezioneMessaggi_formato_anteprima"] = "Anteprima";
	L["opzioni_RicezioneMessaggi_gruppo_messaggi"] = "Messaggi";
	L["opzioni_RicezioneMessaggi_gruppo_achievements"] = "Imprese";
	--Skinner
	L["opzioni_Skinner_Skin"] = "Skin";
	---------------------------------------------------------------------------------------
	-- Fine Opzioni
	---------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------
	-- Comunicazione
	---------------------------------------------------------------------------------------
	L["gxchat_Comunicazione_errore_registraControllo"] = "strIDRisposta (%s) già usato da un'altro modulo";
	---------------------------------------------------------------------------------------
	-- Fine Comunicazione
	---------------------------------------------------------------------------------------
end