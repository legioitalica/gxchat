-- Author      : Galilman
-- Create Date : 13/09/2014 11:26:24

local LibCodifiche = LibStub:NewLibrary("gxchat-LibCodifiche", 1)

if not LibCodifiche then
    return
end

--[[
Sezione codice codifica Base64
]]
-- encryption table
-- Lua 5.1+ base64 v3.0 (c) 2009 by Alex Kloss <alexthkloss@web.de>
-- licensed under the terms of the LGPL2

-- character table string
local base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

-- encoding
function LibCodifiche:ToBase64 (data)
    return ((data:gsub ('.', function (x) 
        local r, b = "", x:byte ();
        for i = 8, 1, -1 do 
			r = r .. (b % 2^i - b%2^(i-1) > 0 and "1" or "0");
		end
        return r;
    end) .. "0000"):gsub('%d%d%d?%d?%d?%d?', function (x)
        if (#x < 6) then 
			return '';
		end
        local c = 0;
        for i = 1, 6 do 
			c = c + (x:sub (i, i) == "1" and 2^(6 - i) or 0);
		end
        return base:sub(c + 1, c + 1);
    end) .. ({"", "==", "=" })[#data % 3 + 1]);
end

-- decoding
function LibCodifiche:FromBase64(data)
    data = string.gsub (data, "[^" .. base .. "=]", "")
    return (data:gsub(".", function(x)
        if (x == "=") then 
			return "";
		end
        local r, f= "", (base:find(x) - 1);
        for i = 6, 1, -1 do 
			r = r .. (f % 2^i - f % 2^(i - 1) > 0 and "1" or "0");
		end
        return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (#x ~= 8) then 
			return "" 
		end
        local c = 0
        for i = 1, 8 do 
			c = c + (x:sub (i, i) == "1" and 2^(8 - i) or 0);
		end
        return string.char(c);
    end))
end
--[[
Fine codifica Base64
]]

--[[
Sezione codice codifica per SendChatMessage
]]
local tabellaDaCodificare = {
	["\000"] = "\008\032",
	["\001"] = "\008\033",
	["\002"] = "\008\034",
	["\003"] = "\008\035",
	["\004"] = "\008\036",
	["\005"] = "\008\037",
	["\006"] = "\008\038",
	["\007"] = "\008\039",
	["\008"] = "\008\040",
	["\009"] = "\008\041",
	["\010"] = "\008\042",
	["\124"] = "\008\043",
	["\128"] = "\008\044",
	["\129"] = "\008\045",
	["\130"] = "\008\046",
	["\131"] = "\008\047",
	["\132"] = "\008\048",
	["\133"] = "\008\049",
	["\134"] = "\008\050",
	["\135"] = "\008\051",
	["\136"] = "\008\052",
	["\137"] = "\008\053",
	["\138"] = "\008\054",
	["\139"] = "\008\055",
	["\140"] = "\008\056",
	["\141"] = "\008\057",
	["\142"] = "\008\058",
	["\143"] = "\008\059",
	["\144"] = "\008\060",
	["\145"] = "\008\061",
	["\146"] = "\008\062",
	["\147"] = "\008\063",
	["\148"] = "\008\064",
	["\149"] = "\008\065",
	["\150"] = "\008\066",
	["\151"] = "\008\067",
	["\152"] = "\008\068",
	["\153"] = "\008\069",
	["\154"] = "\008\070",
	["\155"] = "\008\071",
	["\156"] = "\008\072",
	["\157"] = "\008\073",
	["\158"] = "\008\074",
	["\159"] = "\008\075",
	["\160"] = "\008\076",
	["\161"] = "\008\077",
	["\162"] = "\008\078",
	["\163"] = "\008\079",
	["\164"] = "\008\080",
	["\165"] = "\008\081",
	["\166"] = "\008\082",
	["\167"] = "\008\083",
	["\168"] = "\008\084",
	["\169"] = "\008\085",
	["\170"] = "\008\086",
	["\171"] = "\008\087",
	["\172"] = "\008\088",
	["\173"] = "\008\089",
	["\174"] = "\008\090",
	["\175"] = "\008\091",
	["\176"] = "\008\092",
	["\177"] = "\008\093",
	["\178"] = "\008\094",
	["\179"] = "\008\095",
	["\180"] = "\008\096",
	["\181"] = "\008\097",
	["\182"] = "\008\098",
	["\183"] = "\008\099",
	["\184"] = "\008\100",
	["\185"] = "\008\101",
	["\186"] = "\008\102",
	["\187"] = "\008\103",
	["\188"] = "\008\104",
	["\189"] = "\008\105",
	["\190"] = "\008\106",
	["\191"] = "\008\107",
	["\192"] = "\008\108",
	["\193"] = "\008\109",
	["\194"] = "\008\110",
	["\195"] = "\008\111",
	["\196"] = "\008\112",
	["\197"] = "\008\113",
	["\198"] = "\008\114",
	["\199"] = "\008\115",
	["\200"] = "\008\116",
	["\201"] = "\008\117",
	["\202"] = "\008\118",
	["\203"] = "\008\119",
	["\204"] = "\008\120",
	["\205"] = "\008\121",
	["\206"] = "\008\122",
	["\207"] = "\009\032",
	["\208"] = "\009\033",
	["\209"] = "\009\034",
	["\210"] = "\009\035",
	["\211"] = "\009\036",
	["\212"] = "\009\037",
	["\213"] = "\009\038",
	["\214"] = "\009\039",
	["\215"] = "\009\040",
	["\216"] = "\009\041",
	["\217"] = "\009\042",
	["\218"] = "\009\043",
	["\219"] = "\009\044",
	["\220"] = "\009\045",
	["\221"] = "\009\046",
	["\222"] = "\009\047",
	["\223"] = "\009\048",
	["\224"] = "\009\049",
	["\225"] = "\009\050",
	["\226"] = "\009\051",
	["\227"] = "\009\052",
	["\228"] = "\009\053",
	["\229"] = "\009\054",
	["\230"] = "\009\055",
	["\231"] = "\009\056",
	["\232"] = "\009\057",
	["\233"] = "\009\058",
	["\234"] = "\009\059",
	["\235"] = "\009\060",
	["\236"] = "\009\061",
	["\237"] = "\009\062",
	["\238"] = "\009\063",
	["\239"] = "\009\064",
	["\240"] = "\009\065",
	["\241"] = "\009\066",
	["\242"] = "\009\067",
	["\243"] = "\009\068",
	["\244"] = "\009\069",
	["\245"] = "\009\070",
	["\246"] = "\009\071",
	["\247"] = "\009\072",
	["\248"] = "\009\073",
	["\249"] = "\009\074",
	["\250"] = "\009\075",
	["\251"] = "\009\076",
	["\252"] = "\009\077",
	["\253"] = "\009\078",
	["\254"] = "\009\079",
	["\255"] = "\009\080"
};

function LibCodifiche:ToSendChatMessageEncoded (messaggio)
	local ret = "";
	local char;
	for i = 1, #messaggio do
		char = string.sub (messaggio, i, i);
		ret = ret .. (tabellaDaCodificare[char] and tabellaDaCodificare[char] or char);
	end

	return ret;
end

local tabellaDecodifica = {};

do
	local function table_invert (t)
		local s = {};
		for k, v in pairs(t) do
			s[v] = k;
		end
		return s;
	end

	tabellaDecodifica = table_invert (tabellaDaCodificare);
end

function LibCodifiche:FromSendChatMessageEncoded (messaggio)
	local ret = "";
	local charCorrente;
	local charSuccessivo;
	local index = 1;
	while (index <= #messaggio) do
		charCorrente = string.sub (messaggio, index, index);
		if (charCorrente == "\008" or charCorrente == "\009") then
			charSuccessivo = string.sub (messaggio, index + 1, index + 1);
			ret = ret .. (tabellaDecodifica[charCorrente .. charSuccessivo]);
			index = index + 2;
		else
			ret = ret .. charCorrente;
			index = index + 1;
		end
	end

	return ret;
end
--[[
fine codifica per SendChatMessage
]]