## Author: Legio Italica Addon Team
## Interface: 60200
## Notes: Addon Chat di gilda cross server
## Title: gxchat
## Version: 1.0.1.20160324
## SavedVariables: gxchat_data

## X-Category: Interface Enhancements

embeds.xml
Locales\Locales.xml

gxchat.lua


Comunicazione\ChatGilda.lua
Comunicazione\ControlloVersione.lua
Comunicazione\RicezioneMessaggi.lua

Interfaccia\src\Skinner.lua
Interfaccia\src\ChatGruppi.lua
Interfaccia\src\BottoneMinimappa.lua
Interfaccia\src\GestioneGilde.lua
Interfaccia\src\ControlloMembri.lua

Interfaccia\src\SkinChat\Alliance.lua

Realms\Realm.eu.lua
Realms\Realm.us.lua
