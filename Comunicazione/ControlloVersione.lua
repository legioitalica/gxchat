-- Author      : Galilman
-- Create Date : 11/10/2014 11:26:56

local gxchat = gxchat;
local ControlloVersione = gxchat:NewModule ("ControlloVersione");
local L = LibStub("AceLocale-3.0"):GetLocale("gxchat");

local libComunicazione = LibStub:GetLibrary("gxchat-LibComunicazione");

local db;
local db_default = {
	abilitato = true,
	inizializzato = true
}

local function controllaVersione (tipo, messaggio)
	if (tipo == "gxchat_check_version_request") then
		-- Arrivata richiesta di controllo versione, rispondo con la versione corrente
		local versioneArrivata = gxchat:split (messaggio.versione, ".");
		versioneArrivata = tonumber (versioneArrivata[1]) * 100000 + tonumber (versioneArrivata[2]) * 1000 + tonumber(versioneArrivata[3]);
		if (gxchat:IsDebug ()) then
			ControlloVersione:Print ("Arrivata versione: " .. versioneArrivata);
		end
		local versioneLocale = gxchat:split (gxchat.versione, ".");
		versioneLocale = tonumber (versioneLocale[1]) * 100000 + tonumber (versioneLocale[2]) * 1000 + tonumber(versioneLocale[3]);
		if (versioneArrivata > versioneLocale) then
			DEFAULT_CHAT_FRAME:AddMessage (string.format (L["gxchat_nuova_versione"], messaggio.versione, gxchat.versione));
		else
			local messaggio = {};
			messaggio.versione = gxchat.versione;
			libComunicazione:SendLocalMessage ("gxchat_check_version_response", messaggio);
		end
	elseif (tipo == "gxchat_check_version_response") then
		-- Arrivata versione, controllo se ho una versione aggiornata
		local versioneArrivata = gxchat:split (messaggio.versione, ".");
		versioneArrivata = tonumber (versioneArrivata[1]) * 100000 + tonumber (versioneArrivata[2]) * 1000 + tonumber(versioneArrivata[3]);
		if (gxchat:IsDebug ()) then
			ControlloVersione:Print ("Arrivata versione: " .. versioneArrivata);
		end
		local versioneLocale = gxchat:split (gxchat.versione, ".");
		versioneLocale = tonumber (versioneLocale[1]) * 100000 + tonumber (versioneLocale[2]) * 1000 + tonumber(versioneLocale[3]);
		if (versioneArrivata > versioneLocale) then
			DEFAULT_CHAT_FRAME:AddMessage (string.format (L["gxchat_nuova_versione"], messaggio.versione, gxchat.versione));
			--PlaySound ("");
		end
	end
end

local function msgCallback ()
	ControlloVersione:EffettuaControllo ();
	ControlloVersione:UnregisterMessage ("NUOVO_MESSAGGIO");
end

function ControlloVersione:OnInitialize ()
	if (not gxchat.db.profile.modules.ControlloVersione.inizializzato) then
		gxchat.db.profile.modules.ControlloVersione = gxchat:copiaTable (db_default);
	end

	db = gxchat.db.profile.modules.ControlloVersione;
end

function ControlloVersione:OnEnable ()
	libComunicazione.RegisterMessage (self, "gxchat_check_version_request", controllaVersione);
	libComunicazione.RegisterMessage (self, "gxchat_check_version_response", controllaVersione);

	self:RegisterMessage ("NUOVO_MESSAGGIO", msgCallback);
end

function ControlloVersione:OnDisable ()
end

function ControlloVersione:EffettuaControllo ()
	local messaggio = {};
	messaggio.versione = gxchat.versione;
	libComunicazione:SendLocalMessage ("gxchat_check_version_request", messaggio);
end

--Presente in tutti i moduli per aggiornare le impostazioni dopo un aggiornamento
function ControlloVersione:AggiornaImpostazioni (versioneLocale)
	self:Debug (versioneLocale);
end