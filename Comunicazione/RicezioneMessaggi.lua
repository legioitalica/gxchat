-- Author      : Galilman
-- Create Date : 27/10/2014 16:26:47

local gxchat = gxchat;
local RicezioneMessaggi = gxchat:NewModule ("RicezioneMessaggi", "AceTimer-3.0");

local libComunicazione = LibStub:GetLibrary("gxchat-LibComunicazione");

local libS = LibStub:GetLibrary ("AceSerializer-3.0");
local libEnc = LibStub:GetLibrary ("gxchat-LibCodifiche");
local libSharedMedia = LibStub:GetLibrary ("LibSharedMedia-3.0"); 

local L = LibStub("AceLocale-3.0"):GetLocale("gxchat");

local gildeAssociate;

local chatGruppi;

local db;
local db_default = {
	abilitato = true,
	inizializzato = true,
	chat_type = {
	},
	chat_type_achi = {
	}
}

---------------------------------------------------------------------------------------
-- Integrazione su chat wow
---------------------------------------------------------------------------------------
local db_default_chat_type = {
	abilitato = true,
	colore_classe = true,
	r = 0.12,
	g = 1,
	b = 0.12,
	a = 1,
	formato = "%data% %ora% [%canale%][%player%]: %messaggio%"
}

local db_default_chat_type_achi = {
	abilitato = true,
	r = 0.12,
	g = 1,
	b = 0.12,
	a = 1,
	formato = "%data% %ora% [%canale%]: %achi%"
}

local tabella_formati = {
	["%data% %ora% [%canale%][%player%]: %messaggio%"] = "06-12-2014 19:00:59 [Legio Italica@Anachronos][Galilman]: Test",
	["%ora% [%canale%][%player%]: %messaggio%"] = "19:00:59 [Legio Italica@Anachronos][Galilman]: Test",
	["%data% %ora% [%player%]: %messaggio%"] = "06-12-2014 19:00 [Galilman]: Test",	
	["%ora% [%player%]: %messaggio%"] = "19:00 [Galilman]: Test",
	["[%canale%][%player%]: %messaggio%"] = "[Legio Italica@Anachronos][Galilman]: Test",
	["[%player%]: %messaggio%"] = "[Galilman]: Test",
}

local tabella_formati_achi = {
	["%data% %ora% [%canale%]: %achi%"] = "Data Ora [Canale]: achi",
	["%ora% [%canale%]: %achi%"] = "Ora [Canale]: achi",
	["[%canale%]: %achi%"] = "[Canale]: achi",
	["%data% %ora%: %achi%"] = "Data Ora: achi",
	["%ora%: %achi%"] = "Ora: achi",
	["%achi%"] = "achi",
}

local opzioni_chat = {
	name = "Chat WoW",
	type = "group",
	handler = RicezioneMessaggi,
	childGroups = "tab",
	args = {
	}
}

local function CreaImpostazioni ()
	RicezioneMessaggi:Debug ("entro in CreaImpostazioni");
	wipe (opzioni_chat.args);
	for i = 1, NUM_CHAT_WINDOWS  do
		local tabName = GetChatWindowInfo (i);
		if (not tabName or tabName == "") then
			RicezioneMessaggi:Debug ("TabName non presente???");
		else
			if (not db.chat_type[tabName]) then
				db.chat_type[tabName] = {};
			end
			if (not db.chat_type_achi[tabName]) then
				db.chat_type_achi[tabName] = {};
			end
			RicezioneMessaggi:Debug ("creo impostazioni per: " .. tabName);
			opzioni_chat.args[tabName] = {
				name = tabName,
				type = "group",
				order = i,
				args = {
				}
			}
			local chats = opzioni_chat.args[tabName].args;
			for nome, _ in pairs (gildeAssociate) do
				RicezioneMessaggi:Debug ("Creo Impostazioni per: " .. nome);
				if (not db.chat_type[tabName][nome]) then
					db.chat_type[tabName][nome] = gxchat:copiaTable (db_default_chat_type);
				end
				if (not db.chat_type_achi[tabName][nome]) then
					db.chat_type_achi[tabName][nome] = gxchat:copiaTable (db_default_chat_type_achi);
				end
				chats[nome] = {
					name = nome,
					type = "group",
					inline = true,
					args = {
						gruppo_messaggi = {
							name = L["opzioni_RicezioneMessaggi_gruppo_messaggi"],
							type = "group",
							args = {
								canale_abilitato = {
									name = SHOW,
									type = "toggle",
									get = "GetAbilitato",
									set = "SetAbilitato",
									order = 1
								},
								mostra_colore_classe = {
									name = L["opzioni_RicezioniMessaggi_chat_table_colore_classe"],
									type = "toggle",
									get = "GetColoreClasse",
									set = "SetColoreClasse",
									order = 2
								},
								colore_canale = {
									name = "",
									type = "color",
									get = "GetColor",
									set = "SetColor",
									hasAlpha = true,
									width = "full",
									order = 3
								},
								gruppo_formato = {
									name = L["opzioni_RicezioneMessaggi_formato"],
									type = "group",
									inline = true,
									order = 4,
									args = {
										dropdown = {
											name = "",
											type = "select",
											values = tabella_formati,
											order = 1,
											width = "full",
											get = "GetFormato",
											set = "SetFormato"
										},
										header = {
											name = L["opzioni_RicezioneMessaggi_formato_anteprima"],
											order = 2,
											width = "full",
											type = "header"
										},
										anteprima = {
											name = function (info)
												local formato = db.chat_type[info[1]][info[2]].formato;
												local data = date ("%d-%m-%Y");
												local ora = date ("%H:%M:%S");
												local guildName, _ = GetGuildInfo("player");
												local chat = guildName .. "@" .. GetRealmName();
												local player = UnitName ("player");
												formato = string.gsub (formato, gxchat:literalize ("%data%"), data);
												formato = string.gsub (formato, gxchat:literalize ("%ora%"), ora);
												formato = string.gsub (formato, gxchat:literalize ("%canale%"), chat);
												formato = string.gsub (formato, gxchat:literalize ("%player%"), player);
												formato = string.gsub (formato, gxchat:literalize ("%messaggio%"), "Questo � un messaggio di prova");
												return formato;
											end,
											type = "description",
											order = 3,
											width = "full"
										}
									}
								}
							}
						},
						gruppo_achi = {
							name = L["opzioni_RicezioneMessaggi_gruppo_achievements"],
							type = "group",
							args = {
								canale_abilitato = {
									name = SHOW,
									type = "toggle",
									get = "GetAbilitatoAchi",
									set = "SetAbilitatoAchi",
									order = 1
								},
								colore_canale = {
									name = "",
									type = "color",
									get = "GetColorAchi",
									set = "SetColorAchi",
									hasAlpha = true,
									width = "full",
									order = 3
								},
								gruppo_formato = {
									name = L["opzioni_RicezioneMessaggi_formato"],
									type = "group",
									inline = true,
									order = 4,
									args = {
										dropdown = {
											name = "",
											type = "select",
											values = tabella_formati_achi,
											order = 1,
											width = "full",
											get = "GetFormatoAchi",
											set = "SetFormatoAchi"
										},
										header = {
											name = L["opzioni_RicezioneMessaggi_formato_anteprima"],
											order = 2,
											width = "full",
											type = "header"
										},
										anteprima = {
											name = function (info)
												local formato = db.chat_type_achi[info[1]][info[2]].formato;
												local data = date ("%d-%m-%Y");
												local ora = date ("%H:%M:%S");
												local guildName, _ = GetGuildInfo("player");
												local chat = guildName .. "@" .. GetRealmName();
												local player = UnitName ("player");
												formato = string.gsub (formato, gxchat:literalize ("%data%"), data);
												formato = string.gsub (formato, gxchat:literalize ("%ora%"), ora);
												formato = string.gsub (formato, gxchat:literalize ("%canale%"), chat);
												local msg = string.format (ACHIEVEMENT_BROADCAST, player, "|cffffff00|Hachievement:698:060000000062850B:0:0:0:-1:0:0:0:0|h[Sunwell Plateau]|h|r");
												formato = string.gsub (formato, gxchat:literalize ("%achi%"), msg);
												return formato;
											end,
											type = "description",
											order = 3,
											width = "full"
										}
									}
								}
							}
						},
					}
				}
			end
		end
	end

	local AceConfigRegistry = LibStub("AceConfigRegistry-3.0")
	AceConfigRegistry:NotifyChange ("gxchat_InterfacciaChat opzioni");
end

local function GetColoredName (name, class) 
	RicezioneMessaggi:Debug ("classe: " .. class);
	if (class) then
		local classColorTable = RAID_CLASS_COLORS[class];
		if (not classColorTable) then
			return name;
		end
		return string.format("|cff%.2x%.2x%.2x", classColorTable.r*255, classColorTable.g*255, classColorTable.b*255) .. name .. "|r";
	end
	return name;
end

local function StampaMessaggio (chat, sender, senderClass, messaggio)
	RicezioneMessaggi:Debug ("StampaMessaggio da " .. chat);
	for i = 1, NUM_CHAT_WINDOWS do
		local tabName = GetChatWindowInfo (i);
		local impostazioniTab = db.chat_type[tabName];
		if (impostazioniTab and impostazioniTab[chat] and impostazioniTab[chat].abilitato) then
			local player = sender;
			if (impostazioniTab[chat].colore_classe and player) then
				player = GetColoredName (player, senderClass);
			end
			local formato = impostazioniTab[chat].formato;
			local data = date ("%d-%m-%Y");
			local ora = date ("%H:%M:%S");
			formato = string.gsub (formato, gxchat:literalize ("%data%"), data);
			formato = string.gsub (formato, gxchat:literalize ("%ora%"), ora);
			formato = string.gsub (formato, gxchat:literalize ("%canale%"), chat);
			formato = string.gsub (formato, gxchat:literalize ("%player%"), player);
			formato = string.gsub (formato, gxchat:literalize ("%messaggio%"), messaggio);
			_G["ChatFrame" .. i]:AddMessage (formato, impostazioniTab[chat].r, impostazioniTab[chat].g, impostazioniTab[chat].b);
		end
	end
end

local function StampaMessaggioOfficer (chat, sender, senderClass, messaggio)
	RicezioneMessaggi:Debug ("StampaMessaggioOfficer da " .. chat);
	for i = 1, NUM_CHAT_WINDOWS do
		local tabName = GetChatWindowInfo (i);
		local impostazioniTab = db.chat_type[tabName];
		if (impostazioniTab and impostazioniTab[chat] and impostazioniTab[chat].abilitato) then
			local player = sender;
			if (impostazioniTab[chat].colore_classe and player) then
				player = GetColoredName (player, senderClass);
			end
			local formato = impostazioniTab[chat].formato;
			local data = date ("%d-%m-%Y");
			local ora = date ("%H:%M:%S");
			formato = string.gsub (formato, gxchat:literalize ("%data%"), data);
			formato = string.gsub (formato, gxchat:literalize ("%ora%"), ora);
			formato = string.gsub (formato, gxchat:literalize ("%canale%"), chat);
			formato = string.gsub (formato, gxchat:literalize ("%player%"), "Officer " .. player);
			RicezioneMessaggi:Debug ("Formato: " .. formato .. " messaggio: " .. messaggio);
			formato = string.gsub (formato, gxchat:literalize ("%messaggio%"), messaggio);
			_G["ChatFrame" .. i]:AddMessage (formato, impostazioniTab[chat].r, impostazioniTab[chat].g, impostazioniTab[chat].b);
		end
	end
end

local function StampaAchi (chat, sender, messaggio)
	RicezioneMessaggi:Debug ("Entro in SampaAchi");
	for i = 1, NUM_CHAT_WINDOWS do
		local tabName = GetChatWindowInfo (i);
		RicezioneMessaggi:Debug (tabName);
		local impostazioniTab = db.chat_type_achi[tabName];
		if (impostazioniTab and impostazioniTab[chat] and impostazioniTab[chat].abilitato) then
			RicezioneMessaggi:Debug ("stampo per: " .. chat);
			local colore = string.format("|cff%.2x%.2x%.2x", impostazioniTab[chat].r * 255, impostazioniTab[chat].g * 255, impostazioniTab[chat].b * 255);
			local formato = impostazioniTab[chat].formato;
			local data = date ("%d-%m-%Y");
			local ora = date ("%H:%M:%S");
			formato = string.gsub (formato, gxchat:literalize ("%data%"), data);
			formato = string.gsub (formato, gxchat:literalize ("%ora%"), ora);
			formato = string.gsub (formato, gxchat:literalize ("%canale%"), chat);
			RicezioneMessaggi:Debug (messaggio);
			local msg = string.format (messaggio, sender);
			formato = string.gsub (formato, gxchat:literalize ("%achi%"), msg);
			RicezioneMessaggi:Debug (formato);
			local msg = colore .. string.format (formato, sender);
			RicezioneMessaggi:Debug (msg);
			_G["ChatFrame" .. i]:AddMessage (msg);
		end
	end
end

--Get e Set per le opzioni
function RicezioneMessaggi:GetAbilitato (info)
	return db.chat_type[info[1]][info[2]].abilitato;
end

function RicezioneMessaggi:SetAbilitato (info, enabled)
	db.chat_type[info[1]][info[2]].abilitato = enabled;
end

function RicezioneMessaggi:GetColor (info)
	return db.chat_type[info[1]][info[2]].r, db.chat_type[info[1]][info[2]].g, db.chat_type[info[1]][info[2]].b, db.chat_type[info[1]][info[2]].a;
end

function RicezioneMessaggi:SetColor (info, r, g, b, a)
	if (r and g and b and a) then
		db.chat_type[info[1]][info[2]].r, db.chat_type[info[1]][info[2]].g, db.chat_type[info[1]][info[2]].b, db.chat_type[info[1]][info[2]].a = r, g, b, a;
	end
end

function RicezioneMessaggi:GetColoreClasse (info)
	return db.chat_type[info[1]][info[2]].colore_classe;
end

function RicezioneMessaggi:SetColoreClasse (info, enabled)
	db.chat_type[info[1]][info[2]].colore_classe = enabled;
end

function RicezioneMessaggi:GetFormato (info)
	return db.chat_type[info[1]][info[2]].formato;
end

function RicezioneMessaggi:SetFormato (info, key)
	db.chat_type[info[1]][info[2]].formato = key;
end

function RicezioneMessaggi:GetAbilitatoAchi (info)
	return db.chat_type_achi[info[1]][info[2]].abilitato;
end

function RicezioneMessaggi:SetAbilitatoAchi (info, enabled)
	db.chat_type_achi[info[1]][info[2]].abilitato = enabled;
end

function RicezioneMessaggi:GetColorAchi (info)
	return db.chat_type_achi[info[1]][info[2]].r, db.chat_type[info[1]][info[2]].g, db.chat_type[info[1]][info[2]].b, db.chat_type[info[1]][info[2]].a;
end

function RicezioneMessaggi:SetColorAchi (info, r, g, b, a)
	if (r and g and b and a) then
		db.chat_type_achi[info[1]][info[2]].r, db.chat_type_achi_achi[info[1]][info[2]].g, db.chat_type_achi[info[1]][info[2]].b, db.chat_type_achi[info[1]][info[2]].a = r, g, b, a;
	end
end

function RicezioneMessaggi:GetFormatoAchi (info)
	return db.chat_type_achi[info[1]][info[2]].formato;
end

function RicezioneMessaggi:SetFormatoAchi (info, key)
	db.chat_type_achi[info[1]][info[2]].formato = key;
end
---------------------------------------------------------------------------------------
-- Fine integrazione su chat wow
---------------------------------------------------------------------------------------

local function Messaggio_GILDE_CAMBIATE ()
	if (not gildeAssociate) then
		gildeAssociate = {};
	end
	wipe (gildeAssociate);
	if (IsInGuild ()) then
		local messaggioGilda = GetGuildInfoText ();
		RicezioneMessaggi:Debug (messaggioGilda);
		local gxStart, gxEnd = string.find (messaggioGilda, "## gxchat");
		if (gxStart) then
			local gx = string.sub (messaggioGilda, gxEnd + 2);
			gx = libEnc:FromBase64 (gx);
			local succ, des = libS:Deserialize (gx);
			if (succ) then
				gildeAssociate = des;
			else
				error (des);
			end
		end
		local guildName, guildRankName, guildRankIndex = GetGuildInfo("player");
		local nomeServer = GetRealmName();

		gildeAssociate[guildName .. "@" .. nomeServer] = true;

		CreaImpostazioni ();
	end
end

local switchMessaggiLocali = {
	["GILDE_CAMBIATE"] = Messaggio_GILDE_CAMBIATE
}

local function NUOVO_MESSAGGIO (message)
	RicezioneMessaggi:SendMessage("NUOVO_MESSAGGIO", message);

	StampaMessaggio (message.gilda .. "@" .. message.server, message.player, message.classe, message.messaggio);

	chatGruppi:AggiungiMessaggioGilda (message, "msg");
end

local function NUOVO_ACHIEVEMENT (message)
	RicezioneMessaggi:SendMessage("NUOVO_ACHIEVEMENT", message);

	StampaAchi (message.gilda .. "@" .. message.server, message.player, message.messaggio);

	chatGruppi:AggiungiMessaggioGilda (message, "achi");
end

local function NUOVO_MESSAGGIO_OFFICER (message)
	--Controllo possibilit� di leggere i messaggi degli officer
	if (CanEditOfficerNote ()) then
		StampaMessaggioOfficer (message.gilda .. "@" .. message.server, message.player, message.classe, message.messaggio);

		chatGruppi:AggiungiMessaggioOfficer (message);
	end
end

local function NUOVO_MESSAGGIO_GRUPPI (message)
	RicezioneMessaggi:Debug ("Arrivato " .. message.messaggio .. " su " .. message.gruppo);
	chatGruppi:AggiungiMessaggio (message.gruppo, message);
end

local switchMessaggiRemoti = {
	["gxchat_chat"] = NUOVO_MESSAGGIO,
	["gxchat_achievement"] = NUOVO_ACHIEVEMENT,
	["gxchat_chat_officer"] = NUOVO_MESSAGGIO_OFFICER,
	["gxchat_chat_gruppi"] = NUOVO_MESSAGGIO_GRUPPI
}

local function LocalMessageCallback (message, ...)
	local func = switchMessaggiLocali[message];
	if (func) then
		func (...);
	end
end

local function RemoteMessageCallback (type, message, ...)
	--Caricamento delle gilde associate, non sempre le carica correttamente al login
	if (not gildeAssociate) then
		Messaggio_GILDE_CAMBIATE ();
	else
		local count = 0;
		for _, _ in pairs (gildeAssociate) do
			count = count + 1;
		end
		if (count < 2) then
			Messaggio_GILDE_CAMBIATE ();
		end
	end

	RicezioneMessaggi:Debug ("RemoteMessageCallback() type =" .. type);

	if (type == "gxchat_chat_gruppi") then
		switchMessaggiRemoti[type](message);
	elseif (message.can_read) then
		local guildName, guildRankName, guildRankIndex = GetGuildInfo("player");
		local nomeServer = GetRealmName();
		if (message.can_read[guildName .. "@" .. nomeServer]) then
			switchMessaggiRemoti[type](message);
		end
	elseif (message.gilda and gildeAssociate[(message.gilda .. "@" .. message.server)]) then
		switchMessaggiRemoti[type](message);	
	end
end

function RicezioneMessaggi:OnInitialize ()
	libComunicazione.RegisterMessage (self, "gxchat_chat", RemoteMessageCallback);
	libComunicazione.RegisterMessage (self, "gxchat_achievement", RemoteMessageCallback);
	libComunicazione.RegisterMessage (self, "gxchat_chat_officer", RemoteMessageCallback);
	libComunicazione.RegisterMessage (self, "gxchat_chat_gruppi", RemoteMessageCallback);

	if (not gxchat.db.profile.modules.RicezioneMessaggi.inizializzato) then
		gxchat.db.profile.modules.RicezioneMessaggi = gxchat:copiaTable (db_default);
	end

	db = gxchat.db.profile.modules.RicezioneMessaggi;

	self:RegisterMessage ("GILDE_CAMBIATE", LocalMessageCallback);

	LibStub("AceConfig-3.0"):RegisterOptionsTable("gxchat_RicezioneMessaggi", opzioni_chat);
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("gxchat_RicezioneMessaggi", L["gxchat_opzioni_RicezioneMessaggi_titolo"], "gxchat");
end

function RicezioneMessaggi:OnEnable ()
	RicezioneMessaggi:ScheduleTimer (Messaggio_GILDE_CAMBIATE, 5);
	chatGruppi = gxchat:GetModule ("ChatGruppi");
end

function RicezioneMessaggi:OnDisable ()
end

--Presente in tutti i moduli per aggiornare le impostazioni dopo un aggiornamento
function RicezioneMessaggi:AggiornaImpostazioni (versioneLocale)
	self:Debug (versioneLocale);
	--Versione 0.1.2 (0 * 100000 + 1 * 1000 + 2)
	if (versioneLocale < 1002) then
		db.avviso_sonoro = {
			attivo = false,
			suono = ""
		};
	end
	--Versione 0.2.0 (0 * 100000 + 2 * 1000 + 0)
	if (versioneLocale < 2000) then
		db.chat_type = {
		};
		db.chat_type_achi = {
		};
	end
	--Versione 0.2.183 (0 * 100000 + 2 * 1000 + 183)
	if (versioneLocale < 2183) then
		db.avviso_sonoro = nil;
	end
end

--Ottiene le gilde associate
function RicezioneMessaggi:GetGildeAssociate ()
	if (not gildeAssociate) then
		Messaggio_GILDE_CAMBIATE ();
	end

	return gildeAssociate;
end

function RicezioneMessaggi:Dump ()
end