-- Author      : Galilman
-- Create Date : 15/09/2014 11:56:48

local gxchat = gxchat;
local ChatGilda = gxchat:NewModule ("ChatGilda");

--Localizzazione
local L = LibStub ("AceLocale-3.0"):GetLocale ("gxchat");

local libComunicazione = LibStub:GetLibrary ("gxchat-LibComunicazione");
local chatGruppi;
local ricezioneMesaggi;

local db;
local playerName;
local nomeServer;
local classe;

local db_default = {
	abilitato = true,
	inizializzato = true
};

function ChatGilda:OnInitialize ()
	if (not gxchat.db.profile.modules.ChatGilda.inizializzato) then
		gxchat.db.profile.modules.ChatGilda = gxchat:copiaTable (db_default);
	end

	db = gxchat.db.profile.modules.ChatGilda;
end

--Abilito il modulo
function ChatGilda:OnEnable ()
	nomeServer = GetRealmName();
	playerName = UnitName ("player");
	_, classe = UnitClass ("player");

	self:RegisterEvent ("CHAT_MSG_GUILD");
	self:RegisterEvent ("CHAT_MSG_OFFICER");
	
	--TODO Da aggiungere successivamente
	self:RegisterEvent ("CHAT_MSG_ACHIEVEMENT");

	chatGruppi = gxchat:GetModule ("ChatGruppi");
	ricezioneMesaggi = gxchat:GetModule ("RicezioneMessaggi");
end

--Disabilito il modulo
function ChatGilda:OnDisable()
end

function ChatGilda:CHAT_MSG_GUILD (evento, msg, autore, ...)
	local arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13 = ...;

	if (autore == (playerName .. "-" .. string.gsub (nomeServer, " ", ""))) then
		local guildName, guildRankName, guildRankIndex = GetGuildInfo("player");
		local messaggio = {};
		messaggio.player = playerName;
		messaggio.gilda = guildName;
		messaggio.server = nomeServer;
		messaggio.messaggio = msg;
		messaggio.classe = classe;
		messaggio.can_read = ricezioneMesaggi:GetGildeAssociate ();

		ChatGilda:Debug ("player: " .. playerName .. "\nGilda: " .. (guildName or "nil"));

		libComunicazione:SendMessage ("gxchat_chat", messaggio);
		chatGruppi:AggiungiMessaggioGilda (messaggio, "msg");
	end
end

function ChatGilda:CHAT_MSG_ACHIEVEMENT (evento, msg, autore, ...)
	local arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13 = ...;

	if (autore == (playerName .. "-" .. string.gsub (nomeServer, " ", ""))) then
		local guildName, guildRankName, guildRankIndex = GetGuildInfo("player");
		local messaggio = {};
		messaggio.player = playerName;
		messaggio.gilda = guildName;
		messaggio.server = nomeServer;
		messaggio.messaggio = msg;
		messaggio.classe = classe;
		messaggio.can_read = ricezioneMesaggi:GetGildeAssociate ();

		libComunicazione:SendMessage ("gxchat_achievement", messaggio);
		ChatGilda:SendMessage ("NUOVO_ACHIEVEMENT", messaggio);
		chatGruppi:AggiungiMessaggioGilda (messaggio, "achi");
	end
end

function ChatGilda:CHAT_MSG_OFFICER (evento, msg, autore, ...)
	local arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13 = ...;

	self:Debug ("CHAT_MSG_OFFICER " .. msg .. ", " .. autore .. ", " .. arg3 .. ", " .. ", " .. arg4 .. ", " .. arg5 .. ", " .. arg6 .. ", " .. arg7 .. ", " .. arg8 .. ", " .. arg9 .. ", " .. arg10 .. ", " .. arg11 .. ", " .. arg13);

	if (autore == (playerName .. "-" .. string.gsub (nomeServer, " ", ""))) then
		local guildName, guildRankName, guildRankIndex = GetGuildInfo("player");
		local messaggio = {};
		self:Debug ("Player Name: " .. playerName .. " gilda: " .. guildName .. " server: " .. nomeServer .. " msg: " .. msg .. " classe: " .. classe);
		messaggio.player = playerName;
		messaggio.gilda = guildName;
		messaggio.server = nomeServer;
		messaggio.messaggio = msg;
		messaggio.classe = classe;
		messaggio.can_read = ricezioneMesaggi:GetGildeAssociate ();

		libComunicazione:SendMessage ("gxchat_chat_officer", messaggio);
		chatGruppi:AggiungiMessaggioOfficer (messaggio);
	end
end

--Presente in tutti i moduli per aggiornare le impostazioni dopo un aggiornamento
function ChatGilda:AggiornaImpostazioni (versioneLocale)
end